﻿using System;
using UnityEngine;
using NinJanKenPon.Constant;
using NinJanKenPon.Enum;
using NinJanKenPon.ModeSystem;

namespace NinJanKenPon.WeaponSystem
{
    [RequireComponent(typeof(Collider2D))]
    public class WeaponHitbox : MonoBehaviour
    {
        public delegate void AttackDelegate(Character Attacker, Character Touched);
        public static AttackDelegate OnAttackSucceed;

        bool IsTriggerEnable = true;

        Collider2D Collider2D;

        [Tooltip("Scriptable object that contain the mode's data")]
        public ModeDescriber CurrentMode;

        [HideInInspector] public GameObject Owner;

        public EnumMode Type
        {
            get {
                return CurrentMode.Type;
            }
        }
        public EnumMode[] BeatenBy
        {
            get {
                return CurrentMode.BeatenBy;
            }
        }
        public EnumMode[] Beat
        {
            get {
                return CurrentMode.Beat;
            }
        }

        public static bool operator >(WeaponHitbox A, WeaponHitbox B)
        {
            return Array.IndexOf(A.Beat, B.Type) >= 0;
        }

        public static bool operator <(WeaponHitbox A, WeaponHitbox B)
        {
            return Array.IndexOf(A.BeatenBy, B.Type) >= 0;
        }

        public static bool operator ==(WeaponHitbox A, WeaponHitbox B)
        {
            return A.Type == B.Type;
        }

        public static bool operator !=(WeaponHitbox A, WeaponHitbox B) {
            return A.Type != B.Type;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            
            WeaponHitbox B = (WeaponHitbox)obj;
            return Type == B.Type;
        }
        
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        void Awake()
        {
            Collider2D = GetComponent<Collider2D>();
            Collider2D.enabled = false;
        }

        void Update()
        {
            if (!IsTriggerEnable)
                EnableTrigger();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (!IsTriggerEnable || TagConstant.PLAYER != other.tag)
                return;

            DisableTrigger();

            if (null == Owner || TagConstant.PLAYER != Owner.tag)
                Debug.LogErrorFormat("[Weapon {0}] Noone player is holding the weapon ?! What ?! O.o", name);
            else if (null != OnAttackSucceed)
            {
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[Weapon {0}] {1} OnAttackSucceed on {2}", name, Owner.name, other.name);

                OnAttackSucceed(Owner.GetComponent<Character>(), other.GetComponent<Character>());
            }
        }

        public void EnableTrigger()
        {
            IsTriggerEnable = true;
        }

        public void DisableTrigger()
        {
            IsTriggerEnable = false;
        }
    }
}
