﻿using UnityEngine;

namespace NinJanKenPon.StateSystem
{
    public abstract class State<T> where T : MonoBehaviour
    {
        T owner;
        
        public State<T> PreviousState;
        public T Owner {
            get { return owner; }
            set { owner = value; }
        }

        public string Name {
            get { return this.GetType().Name; }
        }

        public State(T mono) // constructor
        {
            Owner = mono;
        }

        public abstract void Tick();

        public virtual void OnStateEnter() {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[State] {0} enter state {1}", owner.name, Name);
        }

        public virtual void OnStateExit() {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[State] {0} exit state {1}", owner.name, Name);
        }
    }
}