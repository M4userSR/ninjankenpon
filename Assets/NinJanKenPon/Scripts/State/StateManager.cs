﻿
using UnityEngine;

namespace NinJanKenPon.StateSystem
{
    public class StateManager<T> where T : MonoBehaviour
    {
        State<T> currentState;

        public State<T> CurrentState {
            get { return currentState; }
            set { SetState(value); }
        }

        protected virtual void SetState(State<T> state)
        {
            if (currentState != null && currentState.GetType() == state.GetType())
            {
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[StateManager] {0} is already on state {1}", currentState.Owner.name, currentState.Name);

                return;
            }

            if (currentState != null)
            {
                currentState.OnStateExit();
                state.PreviousState = currentState;
            }

            currentState = state;

            if (currentState != null)
                currentState.OnStateEnter();
        }
    }
}
