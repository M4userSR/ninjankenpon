﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

// TODO Handle back previous scene
// TODO Handle create/join room or join random room
namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(SettingManager))]
    public class PhotonJoinManager : MonoBehaviourPunCallbacks
    {
        const int NB_MAX_PLAYER = 4; /// TODO <see cref="LobbyManager"/> NB_MAX_PLAYER
        const string PLAYER_NAME_PREFS_KEY = "PlayerName";
        const string GAME_VERSION = "1.dev"; // TODO charger ceci depuis un fichier de configuration

        public InputField PlayerNameInput = null;
        public Button PlayButton = null;

        SettingManager SettingManager = null;

        void Awake()
        {
            SettingManager = GetComponent<SettingManager>();

            if (SettingManager.IsLoaded && !GlobalManager.Instance.HasSettings)
                GlobalManager.Instance.SettingContainer = SettingManager.Container;
        }

        void Start()
        {
            SetDefaultPlayerName();
        }

        void SetDefaultPlayerName()
        {
            string defaultName = string.Empty;

            if (null != PlayerNameInput && PlayerPrefs.HasKey(PLAYER_NAME_PREFS_KEY))
            {
                defaultName = PlayerPrefs.GetString(PLAYER_NAME_PREFS_KEY);
                PlayerNameInput.text = defaultName;
            }

            PhotonNetwork.NickName = defaultName;
        }

        public override void OnEnable()
        {
            PlayerNameInput.onValueChanged.AddListener(SetPlayerName);
            PlayButton.onClick.AddListener(Connect);
            base.OnEnable();
        }

        public override void OnDisable()
        {
            PlayerNameInput.onValueChanged.RemoveListener(SetPlayerName);
            PlayButton.onClick.RemoveListener(Connect);
            base.OnDisable();
        }

        public override void OnConnectedToMaster()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[PhotonJoinManager.OnConnectedToMaster] Automatically join random room");

            PhotonNetwork.JoinRandomRoom();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("[PhotonJoinManager.OnDisconnected] Reason {0}", cause);
            // TODO Warn user in UI, check some cause that need a message
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            if (Debug.isDebugBuild)
                Debug.Log("[PhotonJoinManager.OnJoinRandomFailed] No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            PhotonNetwork.CreateRoom(null, new RoomOptions { 
                MaxPlayers = NB_MAX_PLAYER,
                CustomRoomProperties = new Hashtable { { "AssignedPlayerNumber", null } }
            });
        }

        public override void OnJoinedRoom()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[PhotonJoinManager.OnJoinedRoom] This client is Master of the new created room.");

            if (SettingManager.IsLoaded && !string.IsNullOrEmpty(SettingManager.Container.GlobalSetting.OnlineLobbyScene))
                SceneManager.LoadScene(SettingManager.Container.GlobalSetting.OnlineLobbyScene);
            else
                Debug.LogError("[PhotonJoinManager] GlobalSetting.OnlineLobbyScene is null or empty");
        }

        public void Connect()
        {
            if (PhotonNetwork.IsConnected)
                return;

            PhotonNetwork.GameVersion = GAME_VERSION;
            PhotonNetwork.ConnectUsingSettings();
        }

        public void SetPlayerName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return;
            
            PhotonNetwork.NickName = name;
            PlayerPrefs.SetString(PLAYER_NAME_PREFS_KEY, name);
        }
    }
}
