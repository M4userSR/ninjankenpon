﻿using System;
using UnityEngine;
using NinJanKenPon.Struct;
using NinJanKenPon.Enum;
using NinJanKenPon.UI.Arena;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(SettingManager))]
    public class ScoreManager : MonoBehaviour
    {
        [Tooltip("Score to reach by a player to win this turn")]
        public int MaxScore = 0;

        [Tooltip("GameObject in the canvas that contain all players panels")]
        [SerializeField] Transform PlayerPanelPool = null;
        [Tooltip("GameObject in the canvas that contain all results panels")]
        [SerializeField] Transform ResultPanelPool = null;

        bool UpdateUI = false;
        PlayerScore[] PlayerScoreList;
        SettingManager SettingManager;

        void Awake()
        {
            SettingManager = GetComponent<SettingManager>();
        }

        void Update()
        {
            if (UpdateUI)
                UpdateScoreUI();
        }

        void UpdateScoreUI()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[ScoreManager] Updating score UI");

            foreach (PlayerScore playerScore in PlayerScoreList)
            {
                PlayerPanel playerPanel = GetPlayerPanel(playerScore.Player);
                int score = playerScore.Score;
                int panelScore = playerPanel.getTotalScore();

                int newScore = score - panelScore;

                for (int i = Math.Abs(newScore); i > 0; i--)
                    if (newScore > 0)
                        playerPanel.AddScoreItem();
                    else
                        playerPanel.RemoveScoreItem();
            }

            UpdateUI = false;
        }

        PlayerPanel GetPlayerPanel(ControllerPlayer player)
        {
            int playerId = player.PlayerNumber;

            foreach (Transform panel in PlayerPanelPool)
            {
                PlayerPanel playerPanel = panel.GetComponent<PlayerPanel>();

                if (playerId == playerPanel.PlayerId)
                    return playerPanel;
            }

            return null;
        }

        PlayerResultPanel GetResultPanel(ControllerPlayer player)
        {
            foreach (Transform Panel in ResultPanelPool)
            {
                PlayerResultPanel resultPanel = Panel.GetComponent<PlayerResultPanel>();

                if (player.PlayerNumber == resultPanel.PlayerId)
                    return resultPanel;
            }

            return null;
        }

        void InitScore(ControllerPlayer[] playerList)
        {
            PlayerScoreList = new PlayerScore[playerList.Length];
            for (int i = 0; i < playerList.Length; i++)
                PlayerScoreList[i] = new PlayerScore(playerList[i]);
        }

        void DisableAllPanel()
        {
            foreach (Transform panel in PlayerPanelPool)
            {
                panel.GetComponent<PlayerPanel>().Reset();
                panel.gameObject.SetActive(false);
            }

            foreach (Transform panel in ResultPanelPool)
                panel.gameObject.SetActive(false);
        }

        void InitUI(ControllerPlayer[] playerList)
        {
            if (Debug.isDebugBuild)
                Debug.Log("[ScoreManager] Score UI Initialization");

            DisableAllPanel();

            for (int i = 0; i < playerList.Length; i++)
            {
                Transform panel = PlayerPanelPool.GetChild(i);
                if (null != panel)
                    ConfigurePlayerPanel(panel, playerList[i]);

                panel = ResultPanelPool.GetChild(i);
                if (null != panel)
                    ConfigureResultPanel(panel, playerList[i]);
            }
        }

        void ConfigurePlayerPanel(Transform panel, ControllerPlayer player)
        {
            PlayerPanel playerPanel = panel.GetComponent<PlayerPanel>();

            playerPanel.PlayerId = player.PlayerNumber;
            playerPanel.SetName(String.Format("Player#{0}", player.PlayerNumber));

            panel.gameObject.SetActive(true);
        }

        void ConfigureResultPanel(Transform panel, ControllerPlayer player)
        {
            PlayerResultPanel playerResultPanel = panel.GetComponent<PlayerResultPanel>();

            playerResultPanel.PlayerId = player.PlayerNumber;
            playerResultPanel.SetName(String.Format("Player#{0}", player.PlayerNumber));
        }
        
        void AddKill(Character player, EnumMode mode)
        {
            ref PlayerScore playerScore = ref GetPlayerScore(player.gameObject);
            playerScore.KillList.Add(mode);
            playerScore.ConsecutiveKill++;
            playerScore.ConsecutiveDeath = 0;
        }

        void AddDeath(Character player, EnumMode mode)
        {
            ref PlayerScore playerScore = ref GetPlayerScore(player.gameObject);
            playerScore.DeathList.Add(mode);
            playerScore.ConsecutiveDeath++;
            playerScore.ConsecutiveKill = 0;
        }

        void AddScore(Character player, int score)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("Add Score {0} for player {1}", score, player.name);

            ref PlayerScore playerScore = ref GetPlayerScore(player.gameObject);
            playerScore.Score += Math.Abs(score);

            if (Debug.isDebugBuild)
                Debug.LogFormat("Total Score : {0} for player {1}", playerScore.Score, player.name);
        }

        void SubstractScore(Character player, int score)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("Substract Score {0} for player {1}", score, player.name);

            ref PlayerScore playerScore = ref GetPlayerScore(player.gameObject);
            int newScore = playerScore.Score - Math.Abs(score);

            playerScore.Score = Mathf.Clamp(newScore, 0, playerScore.Score);

            if (Debug.isDebugBuild)
                Debug.LogFormat("Total Score : {0} for player {1}", playerScore.Score, player.name);
        }

        ref PlayerScore GetPlayerScore(GameObject player)
        {
            CharacterUserControl character = player.GetComponent<CharacterUserControl>();
            int index = Array.FindIndex(PlayerScoreList, element => { return element.Player.PlayerNumber == character.Id; });
            return ref PlayerScoreList[index];
        }

        public void Init(ControllerPlayer[] playerList)
        {
            InitScore(playerList);
            InitUI(playerList);
        }

        public void HandleScore(FightResult fightResult)
        {
            Character winner = fightResult.Winner;
            Character looser = fightResult.Looser;
            
            AddKill(winner, fightResult.KillMode);
            AddDeath(looser, fightResult.KillMode);

            int? winnerScore = winner.CurrentMindState.GetScoreWhenTouch(looser);
            int? looserScore = looser.CurrentMindState.GetScoreWhenTouched(winner);

            AddScore(winner, winnerScore.HasValue ? winnerScore.Value : SettingManager.ArenaRulesSetting.BaseScoreGain);
            SubstractScore(looser, looserScore.HasValue ? looserScore.Value : SettingManager.ArenaRulesSetting.BaseScoreLost);

            UpdateUI = true;
        }

        public int GetPlayerScoreCount(GameObject player)
        {
            return GetPlayerScore(player).Score;
        }

        public int GetPlayerKillCount(GameObject player)
        {
            return GetPlayerScore(player).KillList.Count;
        }

        public int GetPlayerDeathCount(GameObject player)
        {
            return GetPlayerScore(player).DeathList.Count;
        }

        public int GetPlayerConsecutiveKill(GameObject player)
        {
            return GetPlayerScore(player).ConsecutiveKill;
        }

        public int GetPlayerConsecutiveDeath(GameObject player)
        {
            return GetPlayerScore(player).ConsecutiveDeath;
        }

        public bool IsWinner(GameObject player)
        {
            return GetPlayerScoreCount(player) >= MaxScore;
        }

        public void DisplayResult()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[ScoreManager] Display results in UI");

            foreach (PlayerScore playerScore in PlayerScoreList)
            {
                PlayerResultPanel resultPanel = GetResultPanel(playerScore.Player);
                resultPanel.SetKill(playerScore.KillList.Count);
                resultPanel.SetDeath(playerScore.DeathList.Count);
                resultPanel.gameObject.SetActive(true);
            }
        }

        public void HideResult()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[ScoreManager] Hide results from UI");

            foreach (Transform panel in ResultPanelPool)
                panel.gameObject.SetActive(false);
        }
    }
}
