﻿using UnityEngine;
using NinJanKenPon.Constant;
using NinJanKenPon.Struct;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(SettingManager))]
    public class PlayerManager : MonoBehaviour
    {
        [Tooltip("Container of gameobjets in the hierarchy")]
        [SerializeField] Transform PlayerPool = null;
        [Tooltip("List of ControllerPlayer to instantiate")]
        public ControllerPlayer[] PlayerList = null;
        
        GameObject[] SpawnList = null;
        // List<GameObject> CurrentFightList = new List<GameObject>(); // TODO : Delete this feature ?
        SettingManager SettingManager = null;
        
        Vector3 SpawnPosition
        {
            get {
                return SpawnList.Length == 0 ? Vector3.zero : SpawnList[Random.Range(0, SpawnList.Length)].transform.position;
            }
        }

        void Start()
        {
            SubscribeEvents();
        }

        void Awake()
        {
            SettingManager = GetComponent<SettingManager>();
            SpawnList = GameObject.FindGameObjectsWithTag(TagConstant.SPAWN);
            InitPlayerList();
        }

        void SubscribeEvents()
        {
            Character.OnDie += RespawnPlayer;
        }

        void InitPlayerList()
        {
            if (PlayerList.Length > 0 || GlobalManager.Instance.ControllerPlayerList.Count == 0)
                return;
            
            int totalPlayer = GlobalManager.Instance.ControllerPlayerList.Count;

            PlayerList = new ControllerPlayer[totalPlayer];
            
            for (int i = 0; i < totalPlayer; i++)
                PlayerList[i] = GlobalManager.Instance.ControllerPlayerList[i];
        }

        void RespawnPlayer(Transform player) // TODO Voir le lien avec SpawnPlayer quand il y aura une vraie gestion des Controllers (manettes, clavier) + le fait que le cadavre reste en jeu ou non ?
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[PlayerManager] Respawning {0}", player.name);

            player.position = SpawnPosition;
            player.gameObject.SetActive(true);
        }

        GameObject SpawnPlayer(ControllerPlayer player)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[PlayerManager] Spawning {0}", player.Avatar.Prefab.name);

            GameObject newPlayer = Instantiate(player.Avatar.Prefab, SpawnPosition, Quaternion.identity, PlayerPool);
            
            newPlayer.name = string.Format("Player-{0}", player.PlayerNumber);

            CharacterUserControl characterController = newPlayer.GetComponent<CharacterUserControl>();
            characterController.Id = player.PlayerNumber;

            // Override some character references from SettingManager
            Character character = characterController.Character;
            character.ScorePerMindState = SettingManager.ArenaRulesSetting.ScorePerMindState;
            character.ColorPerMindState = SettingManager.ArenaRulesSetting.ColorPerMindState;
            character.SetPaletteColor(player.PaletteIndex);

            return newPlayer;
        }

        // GameObject GetPlayerFighting(GameObject player)
        // {
        //     CharacterUserControl character = player.GetComponent<CharacterUserControl>();
        //     return CurrentFightList.Find(element => {
        //         CharacterUserControl currentCharacter = element.GetComponent<CharacterUserControl>();
        //         return currentCharacter.Id == character.Id;
        //     });
        // }

        public void SpawnPlayers()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[PlayerManager] Spawning all players");

            foreach (ControllerPlayer player in PlayerList)
                SpawnPlayer(player);
        }

        public void RemoveAllPlayers()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[PlayerManager] Removing all players");

            foreach (Transform player in PlayerPool)
                Destroy(player.gameObject);
        }

        // public void PlayerStartFight(GameObject player)
        // {
        //     CurrentFightList.Add(player);
        // }

        // public void PlayerStopFighting(GameObject player)
        // {
        //     CurrentFightList.Remove(player);
        // }

        // public bool PlayerIsFighting(GameObject player)
        // {
        //     return null != GetPlayerFighting(player);
        // }
        
        public FightResult? GetFightResult(Character attacker, Character touched)
        {
            FightResult fightResult = new FightResult();
            fightResult.Winner = attacker;
            fightResult.Looser = touched;

            if (attacker.WeaponHitbox > touched.WeaponHitbox)
            {
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[PlayerManager] SHOBU ARI ! {0} Win ! :)", attacker.name);
            }
            else if (attacker.WeaponHitbox < touched.WeaponHitbox)
            {
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[PlayerManager] SHOBU ARI ! {0} Loose with the wrong weapon ! Shame on you ! :(", attacker.name);

                fightResult.Winner = touched;
                fightResult.Looser = attacker;
            }
            else {

                if (Debug.isDebugBuild)
                    Debug.LogFormat("[PlayerManager] IKIWAKE !");

                return null;
            }

            fightResult.KillMode = fightResult.Winner.WeaponHitbox.Type;

            return fightResult;
        }
    }
}
