﻿using System.Collections;
using UnityEngine;
using NinJanKenPon.Struct;
using NinJanKenPon.WeaponSystem;
using NinJanKenPon.MindState;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(PlayerManager))]
    [RequireComponent(typeof(ScoreManager))]
    [RequireComponent(typeof(SettingManager))]
    public class GameManager : MonoBehaviour
    {
        [Tooltip("Number of seconds we show results before next game step")]
        [SerializeField] int ShowResultSecond = 0;

        bool InitialGameStarted = false;
        bool IsGameRunning = true;
        PlayerManager PlayerManager;
        ScoreManager ScoreManager;
        SettingManager SettingManager;

        void Start()
        {
            SubscribeEvents();
        }

        void Awake()
        {
            PlayerManager = GetComponent<PlayerManager>();
            ScoreManager = GetComponent<ScoreManager>();
            SettingManager = GetComponent<SettingManager>();
        }

        void Update()
        {
            if (SettingManager.IsLoaded && !InitialGameStarted)
                StartGame();
            else if (Debug.isDebugBuild && !SettingManager.IsLoaded)
                Debug.Log("[GameManager] Waiting SettingManager to be loaded before starting game...");
        }

        void SubscribeEvents()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[GameManager] Subscribe events");

            WeaponHitbox.OnAttackSucceed += OnPlayerAttackSucceed;
        }

        void SetMindState(Character winner, Character looser)
        {
            if (winner.CurrentMindState.GetType() == typeof(ShameState))
                winner.CurrentMindState = new NormalState(winner);
            else if (ScoreManager.GetPlayerConsecutiveKill(winner.gameObject) >= SettingManager.ArenaRulesSetting.NbKillsToKillingSpree)
                winner.CurrentMindState = new KillingSpreeState(winner);

            if (looser.CurrentMindState.GetType() == typeof(KillingSpreeState))
                looser.CurrentMindState = new NormalState(looser);
            else if (ScoreManager.GetPlayerConsecutiveDeath(looser.gameObject) >= SettingManager.ArenaRulesSetting.NbDeathsToShame)
                looser.CurrentMindState = new ShameState(looser);
        }

        IEnumerator AfterShowResult()
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[GameManager] Waiting {0} seconds before restart", ShowResultSecond);
   
            yield return new WaitForSeconds(ShowResultSecond);
            RestartGame();
        }

        public void StartGame()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[GameManager] Start game");

            ScoreManager.MaxScore = SettingManager.ArenaRulesSetting.ScoreToReach;

            PlayerManager.SpawnPlayers();
            ScoreManager.Init(PlayerManager.PlayerList);

            InitialGameStarted = true;
        }

        public void RestartGame()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[GameManager] Restart game");

            ScoreManager.HideResult();
            PlayerManager.RemoveAllPlayers();

            StartGame();
            IsGameRunning = true;
        }

        public void StopGame()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[GameManager] Stop game");

            IsGameRunning = false;
            ScoreManager.DisplayResult();
            StartCoroutine("AfterShowResult");
        }

        public void OnPlayerAttackSucceed(Character Attacker, Character Touched)
        {
            if (!IsGameRunning)
                return;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[GameManager] Receive message that {0} successfully attacked {1}. Asking the PlayerManager if we can get a result of this fight...", Attacker.name, Touched.name);

            /**
             * RECUPERATION DU RESULTAT DU COMBAT
             */
            FightResult? potentialFightResult = PlayerManager.GetFightResult(Attacker, Touched);
            if (!potentialFightResult.HasValue) // Egalité
            {
                if (Debug.isDebugBuild)
                {
                    Debug.Log("[GameManager] No fight result (egality), continue game");
                    Debug.LogFormat("[GameManager] Stun & push away player#{0} & player {1}", Attacker.name, Touched.name);
                }

                Attacker.Stun(Attacker.FacingDirectionInverse);
                Touched.Stun(Attacker.FacingDirection);
                return;
            }

            if (Debug.isDebugBuild)
                Debug.Log("[GameManager] We got a fight result");

            FightResult fightResult = potentialFightResult.Value;
            Character winner = fightResult.Winner;
            Character looser = fightResult.Looser;

            if (!SettingManager.ArenaRulesSetting.WeakModeDeath && Attacker == looser)
            {
                Attacker.Stun(Attacker.FacingDirectionInverse);
                return;
            }

            /**
             * LE PERDANT MEURT
             */
            looser.Die();
            /**
             * MISE A JOUR DES SCORES
             */
            ScoreManager.HandleScore(fightResult);
            /**
             * ETAT D'ESPRIT DU PERSONNAGE
             */
            SetMindState(winner, looser);

            /**
             * FIN DE LA PARTIE ?
             */
            if (ScoreManager.IsWinner(winner.gameObject))
            {
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[GameManager] END OF ROUND : {0} IS THE GREAT WINNER", winner.name);

                StopGame();
            }
        }
    }
}
