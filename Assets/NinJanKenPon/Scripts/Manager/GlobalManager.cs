﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using NinJanKenPon.Utils;
using NinJanKenPon.Struct;

namespace NinJanKenPon.Management
{
    public class GlobalManager : Singleton<GlobalManager>
    {
        public bool HasSettings {
            get => SettingContainer.HasValue;
        }
        public SettingContainer? SettingContainer;
        public List<ControllerPlayer> ControllerPlayerList = new List<ControllerPlayer>();

        void LoadScene(string sceneName)
        {
            if (string.IsNullOrEmpty(sceneName))
            {
                Debug.LogWarning("[GlobalManager] LocalGameSceneName from the SettingContainer is empty. No scene will be loaded.");
                return;
            }

            SceneManager.LoadScene(sceneName);
        }

        public void StartLocalGame() // TODO rendre caduque...
        {
            if (!HasSettings)
                return;

            LoadScene(SettingContainer.Value.GlobalSetting.LocalGameScene);
        }

        public void StartOnlineGame()
        {
            if (!HasSettings)
                return;

            LoadScene(SettingContainer.Value.GlobalSetting.OnlineGameScene);
        }


        public void JoinLocalLobby()
        {
            if (!HasSettings)
                return;

            LoadScene(SettingContainer.Value.GlobalSetting.LocalLobbyScene);
        }

        public void JoinOnlineStarter()
        {
            if (!HasSettings)
                return;

            LoadScene(SettingContainer.Value.GlobalSetting.JoinOnlineScene);
        }

        public void JoinOnlineLobby()
        {
            if (!HasSettings)
                return;

            LoadScene(SettingContainer.Value.GlobalSetting.OnlineLobbyScene);
        }
    }
}
