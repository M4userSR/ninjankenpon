﻿using UnityEngine;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(SettingManager))]
    public class MainMenuManager : MonoBehaviour
    {
        SettingManager SettingManager;

        void Awake()
        {
            SettingManager = GetComponent<SettingManager>();

            if (SettingManager.IsLoaded)
                GlobalManager.Instance.SettingContainer = SettingManager.Container;
        }

        public void JoinLocalLobby()
        {
            GlobalManager.Instance.JoinLocalLobby();
        }

        public void JoinOnline()
        {
            GlobalManager.Instance.JoinOnlineStarter();
        }
    }
}
