﻿using UnityEngine;
using NinJanKenPon.Management;
using NinJanKenPon.UI.Assignation;
using NinJanKenPon.Struct;

public class LobbyUIManager : MonoBehaviour
{
        [Tooltip("GameObject in the canvas that contain all players panels")]
        [SerializeField] Transform PlayerPanelPool = null;

        void Awake()
        {
            InitUI();
        }

        void OnEnable()
        {
            LobbyManager.OnPlayerReady += PlayerIsReady;
            LobbyManager.OnPlayerNotReady += PlayerUnready;
            ChooseAvatarManager.OnCancel += UnassignPanel;
            ChooseAvatarManager.OnNextAvatar += ChooseRight;
            ChooseAvatarManager.OnPreviousAvatar += ChooseLeft;
        }

        void OnDisable()
        {
            LobbyManager.OnPlayerReady -= PlayerIsReady;
            LobbyManager.OnPlayerNotReady -= PlayerUnready;
            ChooseAvatarManager.OnCancel -= UnassignPanel;
            ChooseAvatarManager.OnNextAvatar -= ChooseRight;
            ChooseAvatarManager.OnPreviousAvatar -= ChooseLeft;
        }

        void InitUI()
        {
            if (Debug.isDebugBuild)
                Debug.Log("[LobbyUIManager.InitUI]");
            
            for (int i = 0; i < PlayerPanelPool.childCount; i++)
            {
                PlayerPanel playerPanel = PlayerPanelPool.GetChild(i).GetComponent<PlayerPanel>();
                playerPanel.SetName(string.Format("Player#{0}", i+1));
            }
        }

        PlayerPanel GetPlayerPanel(int playerNumber)
        {
            foreach (Transform panel in PlayerPanelPool)
            {
                PlayerPanel playerPanel = panel.GetComponent<PlayerPanel>();

                if (playerPanel.PlayerId != playerNumber)
                    continue;

                return playerPanel;
            }

            return null;
        }

        public void UnassignPanel(ControllerPlayer cp)
        {
            PlayerPanel playerPanel = GetPlayerPanel(cp.PlayerNumber);

            if (null == playerPanel)
                return;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyUIManager.UnassignPanel] Player#{0}", cp.PlayerNumber);
            
            playerPanel.PlayerId = 0;
        }

        public void PlayerIsReady(ControllerPlayer cp)
        {
            PlayerPanel playerPanel = GetPlayerPanel(cp.PlayerNumber);

            if (null == playerPanel)
                return;
            
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyUIManager.PlayerIsReady] Player#{0}", cp.PlayerNumber);

            playerPanel.IsReady = true;
        }

        public void PlayerUnready(ControllerPlayer cp)
        {
            PlayerPanel playerPanel = GetPlayerPanel(cp.PlayerNumber);

            if (null == playerPanel)
                return;
            
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyUIManager.PlayerUnready] Player#{0}", cp.PlayerNumber);

            playerPanel.IsReady = false;
        }

        public void ChooseRight(ControllerPlayer cp)
        {
            PlayerPanel playerPanel = GetPlayerPanel(cp.PlayerNumber);

            if (null == playerPanel)
                return;

            playerPanel.ChooseRight();
        }

        public void ChooseLeft(ControllerPlayer cp)
        {
            PlayerPanel playerPanel = GetPlayerPanel(cp.PlayerNumber);

            if (null == playerPanel)
                return;

            playerPanel.ChooseLeft();
        }

        public void UpdateAllPanelJoinText(string text)
        {
            foreach (Transform panel in PlayerPanelPool)
            {
                PlayerPanel playerPanel = panel.GetComponent<PlayerPanel>();
                playerPanel.UpdatePanelJoinText(text);
            }
        }

        public bool AssignPlayerNumberToPanel(int playerNum)
        {
            for (int i = 0; i < PlayerPanelPool.childCount; i++)
            {
                PlayerPanel playerPanel = PlayerPanelPool.GetChild(i).GetComponent<PlayerPanel>();

                if (playerPanel.IsAssigned)
                    continue;

                playerPanel.PlayerId = playerNum;

                if (Debug.isDebugBuild)
                    Debug.LogFormat("[LobbyUIManager.AssignPlayerNumberToPanel] Player#{0}", playerNum);

                return true;
            }

            return false;
        }

        public Vector3? GetPositionOfPlayerInPanel(int playerNumber)
        {
            PlayerPanel playerPanel = GetPlayerPanel(playerNumber);
            
            if (null == playerPanel)
                return null;

            return playerPanel.transform.position;
        }
}
