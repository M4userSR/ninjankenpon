﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.InputSystem.Controls;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using NinJanKenPon.Struct;
using NinJanKenPon.Constant;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(SettingManager))]
    [RequireComponent(typeof(LobbyUIManager))]
    [RequireComponent(typeof(ChooseAvatarManager))]
    [RequireComponent(typeof(SandboxManager))]
    public class LobbyManager : MonoBehaviourPunCallbacks
    {
        const int NB_MAX_PLAYER = 4; // TODO, rendre ça configurable. Switch/PC 6 joueurs ?
        const int MIN_PLAYER_STARTING_GAME = 1;

        List<int> AssignedPlayerNumberList = new List<int>();
        List<int> AssignedControllerList = new List<int>();
        List<ControllerPlayer> PlayerJoinedList = new List<ControllerPlayer>();
        List<ControllerPlayer> PlayerReadyList = new List<ControllerPlayer>();
        SettingManager SettingManager = null;
        LobbyUIManager LobbyUIManager = null;
        ChooseAvatarManager ChooseAvatarManager = null;

        [Header ("Input settings")]
        public InputAction StartAction = null; // TODO, à remplacer par un compteur ...
        [Header ("Network settings")]
        [Tooltip("Check this if the lobby must work as a local multiplayer")]
        public bool OfflineMode = false;
        [Tooltip("Text displayed for each join panel when we are online")]
        public string OnlinePanelJoinText = "";

        public bool IsOffline {
            get => OfflineMode || !PhotonNetwork.IsConnected;
        }

        public delegate void LobbyManagerControllerPlayerDelegate(ControllerPlayer cp);
        public static LobbyManagerControllerPlayerDelegate OnPlayerJoin;
        public static LobbyManagerControllerPlayerDelegate OnPlayerLeft;
        public static LobbyManagerControllerPlayerDelegate OnPlayerReady;
        public static LobbyManagerControllerPlayerDelegate OnPlayerNotReady;
        public static LobbyManagerControllerPlayerDelegate OnPlayerLeftOverNetwork;

        #region UNITY
        void Awake()
        {
            SettingManager = GetComponent<SettingManager>();
            LobbyUIManager = GetComponent<LobbyUIManager>();
            ChooseAvatarManager = GetComponent<ChooseAvatarManager>();

            PhotonNetwork.OfflineMode = OfflineMode;
            PhotonNetwork.AutomaticallySyncScene = true; // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        }

        void Start()
        {
            if (!SettingManager.IsLoaded)
            {
                Debug.LogError("[LobbyManager.Start] SettingManager is not setup. Load main scene...");
                SceneManager.LoadScene(0);
            }

            if (!OfflineMode && !PhotonNetwork.IsConnected) {
                Debug.LogError("[LobbyManager.Start] We're trying to access the network's lobby without be connected online. Load main scene...");
                SceneManager.LoadScene(0);
            }

            if (IsOffline)
                return;

            LobbyUIManager.UpdateAllPanelJoinText(OnlinePanelJoinText);

            if  (PhotonNetwork.PlayerList.Count() > NB_MAX_PLAYER)
            {
                Debug.LogError("[LobbyManager.Start] Joined room that is already full. Load main scene...");
                SceneManager.LoadScene(0); // TODO Mieux gérer ...
            }

            if (Debug.isDebugBuild)
                Debug.Log("[LobbyManager.Start] Sync. all players in the lobby");

            foreach (Player player in PhotonNetwork.PlayerList)
                JoinLobby(player);
        }

        public override void OnEnable()
        {
            ChooseAvatarManager.OnSubmit += Ready;
            ChooseAvatarManager.OnCancel += LeftLobby;
            ChooseAvatarManager.OnNextAvatar += OnChooseAvatarNext;
            ChooseAvatarManager.OnPreviousAvatar += OnChooseAvatarPrevious;
            SandboxManager.OnPlayerRemoved += LeftLobby;

            if (IsOffline)
            {
                InputUser.onUnpairedDeviceUsed += OnUnpairedDeviceUsed;
                ++InputUser.listenForUnpairedDeviceActivity;
            }
            else
                PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

            if (null != StartAction)
                StartAction.Enable();

            StartAction.performed += _ => {
                if (!IsOffline) // TODO Gérer le chargement de la scène normale...
                    return;

                if (PlayerReadyList.Count < MIN_PLAYER_STARTING_GAME || PlayerReadyList.Count != PlayerJoinedList.Count)
                    return;

                if (!GlobalManager.Instance.SettingContainer.HasValue && SettingManager.IsLoaded)
                    GlobalManager.Instance.SettingContainer = SettingManager.Container;

                GlobalManager.Instance.ControllerPlayerList = PlayerReadyList;
                GlobalManager.Instance.StartLocalGame();
            };

            base.OnEnable();
        }

        public override void OnDisable()
        {
            ChooseAvatarManager.OnSubmit -= Ready;
            ChooseAvatarManager.OnCancel -= LeftLobby;
            ChooseAvatarManager.OnNextAvatar -= OnChooseAvatarNext;
            ChooseAvatarManager.OnPreviousAvatar -= OnChooseAvatarPrevious;
            SandboxManager.OnPlayerRemoved -= LeftLobby;

            if (IsOffline)
            {
                InputUser.onUnpairedDeviceUsed -= OnUnpairedDeviceUsed;
                --InputUser.listenForUnpairedDeviceActivity;
            }
            else
                PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;

            StartAction.Disable();

            base.OnDisable();
        }
        #endregion

        #region PUN Callbacks & Events
        void OnEvent(EventData photonEvent)
        {
            byte code = photonEvent.Code;
            int playerNumber = -1;
            ControllerPlayer cp;
            System.Predicate<ControllerPlayer> predicate = el => el.PlayerNumber == playerNumber;

            switch (code)
            {
                case NetworkConstant.EVENT_AVATAR_CHOICE_NEXT:
                    playerNumber = (int)photonEvent.CustomData;
                    if (!PlayerJoinedList.Exists(predicate))
                        break;
                    cp = PlayerJoinedList.Find(predicate);
                    ChooseAvatarManager.NextAvatar(ref cp);
                    break;
                case NetworkConstant.EVENT_AVATAR_CHOICE_PREVIOUS:
                    playerNumber = (int)photonEvent.CustomData;
                    if (!PlayerJoinedList.Exists(predicate))
                        break;
                    cp = PlayerJoinedList.Find(predicate);
                    ChooseAvatarManager.PreviousAvatar(ref cp);
                    break;
            }
        }

        public override void OnDisconnected(DisconnectCause cause) // TODO Gérer différentes cause de déconnexion ?
        {
            Debug.LogWarningFormat("[LobbyManager.OnDisconnected] Reason: {0}. Loading main scene...", cause);
            SceneManager.LoadScene(0);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.OnPlayerEnteredRoom] {0}#{1}", newPlayer.NickName, newPlayer.ActorNumber);

            if (PhotonNetwork.PlayerList.Count() > NB_MAX_PLAYER)
                return;

            JoinLobby(newPlayer);
        }

        public override void OnPlayerLeftRoom(Player player)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.OnPlayerLeftRoom] {0}#{1}", player.NickName, player.ActorNumber);

            System.Predicate<ControllerPlayer> predicate = el => el.PlayerNetwork == player;

            if (!PlayerJoinedList.Exists(predicate))
            {
                Debug.LogWarning("[LobbyManager.OnPlayerLeftRoom] This player is not found in the player joined list");
                return;
            }

            if (null != OnPlayerLeftOverNetwork)
                OnPlayerLeftOverNetwork(PlayerJoinedList.Find(predicate));
        }

        public override void OnPlayerPropertiesUpdate(Player player, Hashtable changedProps)
        {
            if (player.IsLocal)
                return;

            System.Predicate<ControllerPlayer> predicate = el => el.PlayerNetwork == player;
            if (!PlayerJoinedList.Exists(predicate))
                return;

            ControllerPlayer cp = PlayerJoinedList.Find(predicate);
            object isPlayerReady;
            if (changedProps.TryGetValue(NetworkConstant.PROP_PLAYER_READY, out isPlayerReady))
            {
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[LobbyManager.OnPlayerPropertiesUpdate] Player#{0} is {1}", cp.PlayerNumber, ((bool)isPlayerReady ? "ready" : "not ready"));

                if ((bool)isPlayerReady)
                {
                    ChooseAvatarManager.RemoveAvatarInScene(cp);
                    LobbyUIManager.PlayerIsReady(cp);
                    AddPlayerReady(cp);
                }
            }
        }
        #endregion

        void OnChooseAvatarNext(ControllerPlayer cp)
        {
            if (!PhotonNetwork.IsConnected || null != cp.PlayerNetwork && !cp.PlayerNetwork.IsLocal) // IsLocal is very important, prevent looping events when choosing the avatar
                return;

            SendPhotonRaiseEvent(cp, NetworkConstant.EVENT_AVATAR_CHOICE_NEXT);
        }

        void OnChooseAvatarPrevious(ControllerPlayer cp)
        {
            if (!PhotonNetwork.IsConnected || null != cp.PlayerNetwork && !cp.PlayerNetwork.IsLocal) // IsLocal is very important, prevent looping events when choosing the avatar
                return;

            SendPhotonRaiseEvent(cp, NetworkConstant.EVENT_AVATAR_CHOICE_PREVIOUS);
        }

        void OnUnpairedDeviceUsed(InputControl control)
        {
            // TODO Need to implement a IsDeviceUsableWithPlayerActions like in PlayerInputManager
            if (!(control is ButtonControl) || control.device is Mouse || AssignedControllerList.Contains(control.device.id) || AssignedPlayerNumberList.Count == NB_MAX_PLAYER)
                return;

            JoinLobby(control.device);
        }


        void SendPhotonRaiseEvent(ControllerPlayer cp, byte code)
        {
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions();
            SendOptions sendOptions = new SendOptions { Reliability = true };

            PhotonNetwork.RaiseEvent(code, cp.PlayerNumber, raiseEventOptions, sendOptions);
        }

        void AssignPlayerNumber(int playerNumber)
        {
            if (!IsOffline)
                AssignedPlayerNumberList = GetAssignedPlayerNumberListFromNetwork();

            if (!AssignedPlayerNumberList.Contains(playerNumber))
                AssignedPlayerNumberList.Add(playerNumber);

            if (!IsOffline)
                UpdateAssignedPlayerNumberListOnNetwork(AssignedPlayerNumberList);
        }

        void UnassignPlayerNumber(int playerNumber)
        {
            if (!IsOffline)
                AssignedPlayerNumberList = GetAssignedPlayerNumberListFromNetwork();

            if (AssignedPlayerNumberList.Contains(playerNumber))
                AssignedPlayerNumberList.Remove(playerNumber);

            if (!IsOffline)
                UpdateAssignedPlayerNumberListOnNetwork(AssignedPlayerNumberList);
        }

        void AddPlayerJoin(ControllerPlayer cp)
        {
            PlayerJoinedList.Add(cp);
            if (null != OnPlayerJoin)
                OnPlayerJoin(cp);
        }

        void RemovePlayerJoin(ControllerPlayer cp)
        {
            PlayerJoinedList.Remove(cp);
            if (null != OnPlayerLeft)
                OnPlayerLeft(cp);
        }

        void AddPlayerReady(ControllerPlayer cp)
        {
            PlayerReadyList.Add(cp);
            if (null != OnPlayerReady)
                OnPlayerReady(cp);
        }

        void RemovePlayerReady(ControllerPlayer cp)
        {
            PlayerReadyList.Remove(cp);
            if (null != OnPlayerNotReady)
                OnPlayerNotReady(cp);
        }

        void UnassignDevice(InputDevice device)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.UnassignDevice] {0}#{1}", device.displayName, device.id);

            AssignedControllerList.Remove(device.id);
        }

        void UpdateAssignedPlayerNumberListOnNetwork(List<int> list)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable { { NetworkConstant.PROP_ROOM_PLAYERNUMBER_LIST, list.ToArray() } });
        }

        List<int> GetAssignedPlayerNumberListFromNetwork()
        {
            int[] playerNumberArray = (int[])PhotonNetwork.CurrentRoom.CustomProperties[NetworkConstant.PROP_ROOM_PLAYERNUMBER_LIST];

            if (null == playerNumberArray)
                return new List<int>();

            return playerNumberArray.OfType<int>().ToList();
        }

        int? GetPlayerNumber()
        {
            List<int> list = IsOffline ? AssignedPlayerNumberList : GetAssignedPlayerNumberListFromNetwork();

            for (int playerNumber = 1; playerNumber <= NB_MAX_PLAYER; playerNumber++)
                if (!list.Contains(playerNumber))
                    return playerNumber;

            return null;
        }

        ControllerPlayer LinkPlayer(int playerNumber, InputDevice device) // Local mode
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.LinkPlayer] Device {0}#{1} is player#{2}", device.displayName, device.id, playerNumber);

            ControllerPlayer cp = new ControllerPlayer();
            cp.PlayerNumber = playerNumber;
            cp.Device = device;

            return cp;
        }

        ControllerPlayer LinkPlayer(int playerNumber, Player playerNetwork) // Offline mode
        {
            Debug.LogFormat("[LobbyManager.LinkPlayer] Player over network {0}#{1} is player#{2}", playerNetwork.NickName, playerNetwork.ActorNumber, playerNumber);

            ControllerPlayer cp = new ControllerPlayer();
            cp.PlayerNumber = playerNumber;
            cp.PlayerNetwork = playerNetwork;

            return cp;
        }

        public void JoinLobby(InputDevice device) // Local mode
        {
            int? potentialPlayerNumber = null;
            potentialPlayerNumber = GetPlayerNumber();

            if (!potentialPlayerNumber.HasValue)
            {
                Debug.LogError("[LobbyManager.JoinLobby] No player number is available");
                return;
            }

            AssignPlayerNumber(potentialPlayerNumber.Value);

            if (!LobbyUIManager.AssignPlayerNumberToPanel(potentialPlayerNumber.Value))
            {
                Debug.LogError("[LobbyManager.JoinLobby] No panel in UI found to show the player");
                UnassignPlayerNumber(potentialPlayerNumber.Value);
                return;
            }

            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.JoinLobby] Assign device {0}#{1} to a playable character", device.displayName, device.id);

            AssignedControllerList.Add(device.id);

            ControllerPlayer cp = LinkPlayer(potentialPlayerNumber.Value, device);

            AddPlayerJoin(cp);
        }

        public void JoinLobby(Player playerFromNetwork) // Online mode
        {
            if (IsOffline) // Shouldn't be here
            {
                Debug.LogError("[LobbyManager.JoinLobby] Trying to add a player whitout being online...");
                return;
            }

            int? potentialPlayerNumber = null;
            bool playerFromNetworkHasAlreadyPlayerNumber = false;
            object value;
            
            playerFromNetwork.CustomProperties.TryGetValue(NetworkConstant.PROP_PLAYER_NUMBER, out value);
            playerFromNetworkHasAlreadyPlayerNumber = null != value;

            potentialPlayerNumber = playerFromNetworkHasAlreadyPlayerNumber ? (byte)value : GetPlayerNumber();

            if (!potentialPlayerNumber.HasValue) // TODO à gérer ce qu'il se passe dans ce cas ...
            {
                Debug.LogError("[LobbyManager.JoinLobby] No player number is available");
                return;
            }

            playerFromNetwork.SetCustomProperties(new Hashtable() { { NetworkConstant.PROP_PLAYER_NUMBER, (byte)potentialPlayerNumber.Value } });
            
            if (playerFromNetwork.IsLocal)
                PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable { { NetworkConstant.PROP_PLAYER_READY, false } });

            if (!playerFromNetworkHasAlreadyPlayerNumber)
                AssignPlayerNumber(potentialPlayerNumber.Value);

            if (!LobbyUIManager.AssignPlayerNumberToPanel(potentialPlayerNumber.Value))
            {
                Debug.LogError("[LobbyManager.JoinLobby] No panel in UI found to show the player");
                UnassignPlayerNumber(potentialPlayerNumber.Value);

                if (playerFromNetwork.IsLocal)
                {
                    Debug.LogError("[LobbyManager.JoinLobby] A problem occured during panel assignation in online mode for this client. We force this client leaving the room. Load main scene...");
                    SceneManager.LoadScene(0); // Mieux gérer ...
                }

                return;
            }

            ControllerPlayer cp = LinkPlayer(potentialPlayerNumber.Value, playerFromNetwork);
            AddPlayerJoin(cp);
        }

        public void LeftLobby(ControllerPlayer cp)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.LeftLobby] Player#{0}", cp.PlayerNumber);

            UnassignPlayerNumber(cp.PlayerNumber);
            if (null != cp.Device)
                UnassignDevice(cp.Device);

            RemovePlayerReady(cp);
            RemovePlayerJoin(cp);
        }

        public void Ready(ControllerPlayer cp)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[LobbyManager.Ready] Player#{0}", cp.PlayerNumber);

            if (!IsOffline && null != cp.PlayerNetwork && cp.PlayerNetwork.IsLocal)
                PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable { { NetworkConstant.PROP_PLAYER_READY, true } });

            AddPlayerReady(cp);
        }
    }
}
