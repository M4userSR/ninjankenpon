﻿using UnityEngine;
using UnityEngine.InputSystem.PlayerInput;
using NinJanKenPon.Struct;
using NinJanKenPon.AvatarSystem;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(LobbyUIManager))]
    public class ChooseAvatarManager : MonoBehaviour
    {
        LobbyUIManager LobbyUIManager = null;

        [SerializeField][Range (1, 10)] int AvatarSizeFactor = 1;
        [SerializeField] Transform AvatarPool = null;
        [SerializeField] AvatarDescriber[] AvatarList = null;
        [SerializeField] GameObject AvatarPrefab = null;

        public delegate void ChooseAvatarControllerPlayerDelegate(ControllerPlayer cp);
        public static ChooseAvatarControllerPlayerDelegate OnSubmit;
        public static ChooseAvatarControllerPlayerDelegate OnCancel;
        public static ChooseAvatarControllerPlayerDelegate OnNextAvatar;
        public static ChooseAvatarControllerPlayerDelegate OnPreviousAvatar;

        void Awake()
        {
            LobbyUIManager = GetComponent<LobbyUIManager>();
        }

        void OnEnable()
        {
            LobbyManager.OnPlayerJoin += SpawnAvatarInScene;
            LobbyManager.OnPlayerLeftOverNetwork += Cancel;
        }

        void OnDisable()
        {
            LobbyManager.OnPlayerJoin -= SpawnAvatarInScene;
            LobbyManager.OnPlayerLeftOverNetwork -= Cancel;
        }

        void SpawnAvatarInScene(ControllerPlayer cp)
        {
            Vector3? potentialPosition = LobbyUIManager.GetPositionOfPlayerInPanel(cp.PlayerNumber);

            if (!potentialPosition.HasValue)
                return;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[ChooseAvatarManager.SpawnAvatarInScene] Player#{0}", cp.PlayerNumber);

            AvatarDescriber avatarToSpawn = AvatarList[cp.AvatarIndex];
            Vector3 avatarPos = potentialPosition.Value;

            InstantiateAvatar(cp, avatarToSpawn, avatarPos);
        }

        void ChangeAvatarInScene(ControllerPlayer cp)
        {
            Transform avatar = GetAvatar(cp.PlayerNumber);

            if (null == avatar)
                return;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[ChooseAvatarManager.ChangeAvatarInScene] Player#{0}", cp.PlayerNumber);

            AvatarDescriber avatarToSpawn = AvatarList[cp.AvatarIndex];
            avatar.GetComponent<SpriteRenderer>().sprite = avatarToSpawn.Sprite;
        }

        void InstantiateAvatar(ControllerPlayer cp, AvatarDescriber avatar, Vector3 positionOfSpawn)
        {
            GameObject newAvatar = GetInstantiatedAvatar(cp);
            newAvatar.name = avatar.name;
            newAvatar.GetComponent<SpriteRenderer>().sprite = avatar.Sprite; // TODO Au lieu d'une image cela pourrait être le personnage en IDLE (sans contrôle possible)
            newAvatar.transform.SetParent(AvatarPool);
            newAvatar.transform.position = positionOfSpawn;
            newAvatar.transform.localScale *= AvatarSizeFactor;

            AvatarLobby avatarLobby = newAvatar.GetComponent<AvatarLobby>();
            avatarLobby.Initialize(cp);
            avatarLobby.OnChoiceNext += () => NextAvatar(ref cp);
            avatarLobby.OnChoicePrevious += () => PreviousAvatar(ref cp);
            avatarLobby.OnChoiceSubmit += () => Submit(cp);
            avatarLobby.OnChoiceCancel += () => Cancel(cp);
        }

        void DestroyAvatar(GameObject avatar)
        {
            Destroy(avatar);
        }

        GameObject GetInstantiatedAvatar(ControllerPlayer cp)
        {
            return null != cp.PlayerNetwork && cp.PlayerNetwork.IsLocal || null == cp.PlayerNetwork ? 
                PlayerInput.Instantiate(AvatarPrefab, pairWithDevice: cp.Device).gameObject :
                Instantiate(AvatarPrefab).gameObject;
            ;
        }

        Transform GetAvatar(int playerNumber)
        {
            Transform avatarFound = null;
            Vector3? potentialPosition = LobbyUIManager.GetPositionOfPlayerInPanel(playerNumber);

            if (!potentialPosition.HasValue)
                return avatarFound;

            Vector3 avatarPos = potentialPosition.Value;
            foreach (Transform avatar in AvatarPool)
            {
                if ((Vector2)avatarPos != (Vector2)avatar.position)
                    continue;

                avatarFound = avatar;
                break;
            }

            return avatarFound;
        }

        public void Submit(ControllerPlayer cp)
        {
            if (!RemoveAvatarInScene(cp))
                return;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[ChooseAvatarManager.Submit] Player#{0}", cp.PlayerNumber);

            cp.Avatar = AvatarList[cp.AvatarIndex]; // TODO Meilleure façon ? Voir pour stocker directement l'avatar à chaque fois et se passer de AvatarIndex

            if (null != OnSubmit)
                OnSubmit(cp);
        }

        public void Cancel(ControllerPlayer cp)
        {
            if (!RemoveAvatarInScene(cp))
                return;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[ChooseAvatarManager.Cancel] Player#{0}", cp.PlayerNumber);

            if (null != OnCancel)
                OnCancel(cp);
        }

        public void PreviousAvatar(ref ControllerPlayer cp)
        {
            cp.AvatarIndex = (cp.AvatarIndex == 0 ? AvatarList.Length - 1 : cp.AvatarIndex - 1);
            ChangeAvatarInScene(cp);

            if (null != OnPreviousAvatar)
                OnPreviousAvatar(cp);
        }

        public void NextAvatar(ref ControllerPlayer cp)
        {
            cp.AvatarIndex = (cp.AvatarIndex == AvatarList.Length - 1 ? 0 : cp.AvatarIndex + 1);
            ChangeAvatarInScene(cp);

            if (null != OnNextAvatar)
                OnNextAvatar(cp);
        }

        public bool RemoveAvatarInScene(ControllerPlayer cp)
        {
            Transform avatar = GetAvatar(cp.PlayerNumber);

            if (null == avatar)
                return false;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[ChooseAvatarManager.RemoveAvatarInScene] Player#{0}", cp.PlayerNumber);

            DestroyAvatar(avatar.gameObject);

            return true;
        }
    }
}
