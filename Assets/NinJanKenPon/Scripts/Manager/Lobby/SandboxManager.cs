﻿using UnityEngine;
using UnityEngine.InputSystem.PlayerInput;
using Photon.Pun;
using NinJanKenPon.Struct;
using NinJanKenPon.Utils;

namespace NinJanKenPon.Management
{
    [RequireComponent(typeof(LobbyUIManager))]
    public class SandboxManager : Singleton<SandboxManager>
    {
        [SerializeField] GameObject SandboxPrefab = null;
        [SerializeField] Transform SandboxPool = null;
        
        LobbyUIManager LobbyUIManager = null;

        public delegate void SandboxManagerControllerPlayerDelegate(ControllerPlayer cp);
        public static SandboxManagerControllerPlayerDelegate OnPlayerRemoved;

        void Awake()
        {
            LobbyUIManager = GetComponent<LobbyUIManager>();
            LobbyManager.OnPlayerLeftOverNetwork += RemovePlayer;
        }

        void OnEnable()
        {
            LobbyManager.OnPlayerReady += SpawnPlayer;
        }

        void OnDisable()
        {
            LobbyManager.OnPlayerReady -= SpawnPlayer;
        }

        void SpawnPlayer(Transform sandbox, ControllerPlayer cp)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[SandboxManager.SpawnPlayer] Player#{0}", cp.PlayerNumber);

            GameObject player = (null == cp.PlayerNetwork) ? 
                PlayerInput.Instantiate(cp.Avatar.Prefab, cp.PlayerNumber, pairWithDevice: cp.Device).gameObject : // offline, paired with a specific device
                PhotonNetwork.Instantiate(cp.Avatar.Prefab.name, Vector2.zero, Quaternion.identity); // online, paired with first device automatically handled by playerInput

            player.transform.position = sandbox.position;
            player.transform.SetParent(sandbox);

            CharacterUserControl characterUserControl = player.GetComponent<CharacterUserControl>();
            characterUserControl.Id = cp.PlayerNumber;
        }

        public void SpawnPlayer(ControllerPlayer cp)
        {
            Transform sandbox = CreateSandbox(cp);

            if (null == cp.PlayerNetwork || cp.PlayerNetwork.IsLocal)
                SpawnPlayer(sandbox, cp);
        }

        public void RemovePlayer(ControllerPlayer cp)
        {
            foreach (Transform sandbox in SandboxPool)
            {
                CharacterUserControl characterUserControl = sandbox.GetComponentInChildren<CharacterUserControl>();
                
                if (null != characterUserControl && cp.PlayerNumber != characterUserControl.Id)
                    continue;
                
                if (Debug.isDebugBuild)
                    Debug.LogFormat("[SandboxManager.RemovePlayer] Player#{0}", cp.PlayerNumber);

                Destroy(sandbox.gameObject);

                if (null != OnPlayerRemoved)
                    OnPlayerRemoved(cp);

                break;
            }
        }

        public Transform CreateSandbox(ControllerPlayer cp)
        {
            Vector3? potentialPos = LobbyUIManager.GetPositionOfPlayerInPanel(cp.PlayerNumber);

            if (!potentialPos.HasValue)
                return null;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[SandboxManager.CreateSandbox] Player#{0}", cp.PlayerNumber);

            return Instantiate(SandboxPrefab, potentialPos.Value, Quaternion.identity, SandboxPool).transform;
        }
    }
}
