﻿using UnityEngine;
using NinJanKenPon.Struct;
using NinJanKenPon.Setting;

namespace NinJanKenPon.Management
{
    public class SettingManager : MonoBehaviour
    {
        bool Loaded = false;

        public bool IsLoaded {
            get => Loaded;
        }

        public SettingContainer Container;
        public GlobalSettingDescriber GlobalSetting {
            get { return Container.GlobalSetting; }
        }
        public ArenaRulesSettingDescriber ArenaRulesSetting {
            get { return Container.ArenaRulesSetting; }
        }

        void Awake()
        {
            if (GlobalManager.Instance.SettingContainer.HasValue)
                Container = GlobalManager.Instance.SettingContainer.Value;

            if (!Loaded && null != GlobalSetting)
                Loaded = true;
            else if (null == GlobalSetting)
                Debug.LogError("[SettingManager] Missing game settings...");
        }
    }
}
