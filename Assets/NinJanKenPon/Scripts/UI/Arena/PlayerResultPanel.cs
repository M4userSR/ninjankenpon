﻿using UnityEngine;
using UnityEngine.UI;

namespace NinJanKenPon.UI.Arena
{
    public class PlayerResultPanel : MonoBehaviour
    {
        [Tooltip("PlayerID to link with a current player")]
        public int PlayerId;
        [Tooltip("Text in the panel where to display the player's name")]
        [SerializeField] Text PlayerNameText = null;
        [Tooltip("Text in the panel where to display the number of kills")]
        [SerializeField] Text NumberKillText = null;
        [Tooltip("Text in the panel where to display the number of kills")]
        [SerializeField] Text NumberDeathText = null;

        public void SetName(string name)
        {
            PlayerNameText.text = name;
        }

        public void SetKill(int count)
        {
            NumberKillText.text = string.Format("{0} Kills", count);
        }

        public void SetDeath(int count)
        {
            NumberDeathText.text = string.Format("{0} Deaths", count);
        }
    }
}
