﻿using UnityEngine;
using UnityEngine.UI;

namespace NinJanKenPon.UI.Arena
{
    public class PlayerPanel : MonoBehaviour
    {
        [Tooltip("PlayerID to link with a current player")]
        public int PlayerId;
        [Tooltip("Text in the player panel where to display the player's name")]
        [SerializeField] Text PlayerText = null;
        [Tooltip("GameObject in the player panel where to spawn a score item")]
        [SerializeField] GameObject ScoreContainer = null;
        [Tooltip("GameObject that represent the score item to spawn")]
        [SerializeField] GameObject ScoreItemPrefab = null;

        public void SetName(string name)
        {
            PlayerText.text = name;
        }

        public void Reset()
        {
            foreach (Transform scoreItem in ScoreContainer.transform)
                Destroy(scoreItem.gameObject);
        }

        public void AddScoreItem()
        {
            Instantiate(ScoreItemPrefab, ScoreContainer.transform.position, Quaternion.identity, ScoreContainer.transform);
        }

        public void RemoveScoreItem()
        {
            int nbScoreItem = ScoreContainer.transform.childCount;

            if (nbScoreItem == 0)
                return;

            Transform scoreItem = ScoreContainer.transform.GetChild(nbScoreItem - 1);
            scoreItem.SetParent(null, false); // Destroy is deffered so we need to remove parent and get childCount updated immediatly if the function is used in a loop

            Destroy(scoreItem.gameObject);
        }

        public int getTotalScore()
        {
            return ScoreContainer.transform.childCount;
        }
    }
}
