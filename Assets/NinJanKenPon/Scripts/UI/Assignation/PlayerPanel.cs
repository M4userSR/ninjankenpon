﻿using UnityEngine;
using UnityEngine.UI;

namespace NinJanKenPon.UI.Assignation
{
    [RequireComponent(typeof(Animator))]
    public class PlayerPanel : MonoBehaviour
    {
        [Tooltip("PlayerID to link with a current player")]
        public int PlayerId = 0;
        [Tooltip("Text in the player panel where to display the player's name")]
        [SerializeField] Text PlayerText = null;
        [Tooltip("Panel in the player panel that contain 'How to join' instructions")]
        [SerializeField] Transform PanelJoin = null;
        [Tooltip("Panel in the player panel that contain 'Choose avatar' instructions")]
        [SerializeField] Transform PanelChooseAvatar = null;
        
        Animator Animator;

        public bool IsAssigned
        {
            get { return PlayerId > 0; }
        }

        public bool IsReady = false;

        void Awake()
        {
            // Setting up references
            Animator = GetComponent<Animator>();
        }

        void Update()
        {
            PanelJoin.gameObject.SetActive(!IsAssigned);
            PanelChooseAvatar.gameObject.SetActive(IsAssigned && !IsReady);
        }

        public void SetName(string name)
        {
            PlayerText.text = name;
        }

        public void ChooseLeft()
        {
            Animator.SetTrigger("Left");
        }

        public void ChooseRight()
        {
            Animator.SetTrigger("Right");
        }

        public void UpdatePanelJoinText(string text)
        {
            PanelJoin.GetComponentInChildren<Text>().text = text;
        }
    }
}
