﻿using UnityEngine;

namespace NinJanKenPon.Setting
{
    [CreateAssetMenu(fileName = "newGlobalSettings", menuName = "NinJanKenPon/Global Settings")]
    public class GlobalSettingDescriber : ScriptableObject
    {
        [Header("Scenes parameters")]
        public string LocalLobbyScene = null;
        public string LocalGameScene = null;
        public string JoinOnlineScene = null;
        public string OnlineLobbyScene = null;
        public string OnlineGameScene = null;
    }
}
