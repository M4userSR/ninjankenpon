﻿using System.Collections.Generic;
using UnityEngine;
using NinJanKenPon.Struct;
using NinJanKenPon.Enum;

namespace NinJanKenPon.Setting
{
    [CreateAssetMenu(fileName = "newArenaRulesSettings", menuName = "NinJanKenPon/Arena's rules settings")]
    public class ArenaRulesSettingDescriber : ScriptableObject
    {
        [Header("Scoring rules")]
        public int ScoreToReach = 10;
        public int BaseScoreGain = 1;
        public int BaseScoreLost = 0;

        [Header("Mind State rules")]
        public int NbKillsToKillingSpree = 3; // TODO, rendre configurable par state ?
        public int NbDeathsToShame = 3; // TODO, rendre configurable par state ?
        public List<MindStateColor> ColorPerMindState = new List<MindStateColor>();
        public List<MindStateScoring> ScorePerMindState = new List<MindStateScoring>();

        [Header("Combat rules")]
        public bool WeakModeDeath = true;
    }
}
