﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class PaletteSwapTextureGenerator : EditorWindow
{
    Texture2D PaletteTexture;

    string PaletteTexturePath
    {
        get => AssetDatabase.GetAssetPath(PaletteTexture);
    }

    [MenuItem("Nin'JanKenPon/Palette texture generator")]
    public static void ShowWindow()
    {
        GetWindow<PaletteSwapTextureGenerator>("Palette texture generator");
    }
    
    void OnGUI()
    {
        GUILayout.Label(
            new GUIContent(
                "Palette texture",
                @"Texture2D of the palette. Used from the generation.
                 It must be a texture with each color of the one palette per column, each variation per row.
                 The first row is the default palette color. Each color must have a unique RED channel value."
            ),
            EditorStyles.boldLabel
        );
        PaletteTexture = (Texture2D)EditorGUILayout.ObjectField(
            GUIContent.none,
            PaletteTexture,
            typeof(Texture2D),
            false
        );

        if (GUILayout.Button("Generate swap texture from palette texture"))
            Generate();
    }

    void InitTextureColor(ref Texture2D texture)
    {
        for (int i = 0; i < texture.width; ++i)
            for (int j = 0; j < texture.height; ++j)
                texture.SetPixel(i, j, new Color(0.0f, 0.0f, 0.0f, 0.0f));

        texture.Apply();
    }

    Texture2D CreateColorSwapTexture(Texture2D fromTexture)
    {
        Texture2D texture = new Texture2D(256, PaletteTexture.height, TextureFormat.RGBA32, false, false);
        texture.filterMode = FilterMode.Point;
        InitTextureColor(ref texture);

        int[] SwapIndex = new int[PaletteTexture.width];

        for (int i = 0; i < PaletteTexture.width; i++) // First line is the default palette
            SwapIndex[i] = ((Color32)PaletteTexture.GetPixel(i, PaletteTexture.width - 1)).r;

        for (int i = 0; i < PaletteTexture.width; i++)
            for (int j = 0; j < PaletteTexture.height; j++)
                texture.SetPixel(SwapIndex[i], j, PaletteTexture.GetPixel(i, j));

        texture.Apply();

        return texture;
    }

    bool Save(string filename, Texture2D texture)
    {
        File.WriteAllBytes(filename, texture.EncodeToPNG());
        AssetDatabase.Refresh();
        return true;
    }

    string GetNewFilename(Texture2D fromTexture)
    {
        string dir = Path.GetDirectoryName(PaletteTexturePath);
        string name = Path.GetFileNameWithoutExtension(PaletteTexturePath);
        return dir + Path.DirectorySeparatorChar + name + "_ColorSwap.png";
    }

    void Generate()
    {
        if (null == PaletteTexture)
        {
            Debug.LogError("[PaletteSwapTextureGenerator.Generate] The palette texture is empty");
            return;
        }

        Texture2D colorSwapTexture = CreateColorSwapTexture(PaletteTexture);
        Save(GetNewFilename(PaletteTexture), colorSwapTexture);
    }
}
