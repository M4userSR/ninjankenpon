﻿namespace NinJanKenPon.Constant
{
    public static class TagConstant
    {
        public const string PLAYER = "Player";
        public const string WEAPON = "Weapon";
        public const string SPAWN = "Respawn";
    }
}