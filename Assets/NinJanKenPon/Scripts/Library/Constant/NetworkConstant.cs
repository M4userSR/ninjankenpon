﻿namespace NinJanKenPon.Constant
{
    public static class NetworkConstant
    {
        public const string PROP_ROOM_PLAYERNUMBER_LIST = "PNL";
        public const string PROP_PLAYER_NUMBER = "PN";
        public const string PROP_PLAYER_READY = "RDY";
        public const byte EVENT_AVATAR_CHOICE_PREVIOUS = 1;
        public const byte EVENT_AVATAR_CHOICE_NEXT = 2;
    }
}