﻿namespace NinJanKenPon.Enum {
    public enum EnumMode { Rock, Paper, Scissor };
    public enum EnumJump { Classic, Wall };
    public enum EnumMindState { Normal, KillingSpree, Shame };
    public enum EnumMovementState { Ground, Air, Wall, Stun };
}