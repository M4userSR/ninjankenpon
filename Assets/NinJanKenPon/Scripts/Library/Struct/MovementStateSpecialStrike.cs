﻿using System.Collections.Generic;
using NinJanKenPon.Enum;

namespace NinJanKenPon.Struct
{
    [System.Serializable]
    public struct MovementStateSpecialStrike
    {
        public EnumMovementState MovementState;
        public List<ModeDirection> ModeDirectionList;
    }
}