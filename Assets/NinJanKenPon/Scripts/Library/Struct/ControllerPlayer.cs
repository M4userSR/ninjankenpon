﻿using UnityEngine;
using UnityEngine.InputSystem;
using Photon.Realtime;
using NinJanKenPon.AvatarSystem;

namespace NinJanKenPon.Struct
{
    [System.Serializable]
    public struct ControllerPlayer
    {
        [Tooltip ("Device used in local mode")]
        public InputDevice Device;

        [Tooltip ("Player number in the game, used in both mode")]
        public int PlayerNumber;

        [Tooltip ("Used only for switching avatar in ChooseAvatarManager")]
        public int AvatarIndex;

        [Tooltip ("Index color variation for the avatar")]
        public int PaletteIndex;

        [Tooltip ("Avatar choosen which will be used IG")]
        public AvatarDescriber Avatar;

        [Tooltip ("Player from Photon, used in online mode")]
        public Player PlayerNetwork;
    }
}