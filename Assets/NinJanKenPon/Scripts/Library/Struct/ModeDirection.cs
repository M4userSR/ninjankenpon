﻿using System.Collections.Generic;
using UnityEngine;
using NinJanKenPon.Enum;

namespace NinJanKenPon.Struct
{
    [System.Serializable]
    public struct ModeDirection
    {
        public EnumMode Mode;
        [Tooltip("1 = up, 0 = left/right, -1 = down, let empty for any direction")]
        public List<int> DirectionAvailable;
    }
}