﻿using System.Collections.Generic;
using NinJanKenPon.Enum;

namespace NinJanKenPon.Struct
{
    public struct PlayerScore
    {
        public int Score;
        public int ConsecutiveKill;
        public int ConsecutiveDeath;
        public List<EnumMode> KillList;
        public List<EnumMode> DeathList;
        public ControllerPlayer Player;

        public PlayerScore(ControllerPlayer player)
        {
            Score = 0;
            ConsecutiveKill = 0;
            ConsecutiveDeath = 0;
            KillList = new List<EnumMode>();
            DeathList = new List<EnumMode>();
            Player = player;
        }
    }
}