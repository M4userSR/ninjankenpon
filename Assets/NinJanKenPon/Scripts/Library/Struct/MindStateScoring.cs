﻿using NinJanKenPon.Enum;

namespace NinJanKenPon.Struct
{
    [System.Serializable]
    public struct MindStateScoring
    {
        public EnumMindState AttackerState;
        public EnumMindState TouchedState;
        public int ScoreGain;
        public int ScoreLost;
    }
}