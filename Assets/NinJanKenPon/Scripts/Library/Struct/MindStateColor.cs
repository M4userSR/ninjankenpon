﻿using UnityEngine;
using NinJanKenPon.Enum;

namespace NinJanKenPon.Struct
{
    [System.Serializable]
    public struct MindStateColor
    {
        public EnumMindState State;
        public Color Color;
    }
}