﻿using NinJanKenPon.Setting;

namespace NinJanKenPon.Struct
{
    [System.Serializable]
    public struct SettingContainer
    {
        public GlobalSettingDescriber GlobalSetting;
        public ArenaRulesSettingDescriber ArenaRulesSetting;
    }
}