﻿using NinJanKenPon.Enum;

namespace NinJanKenPon.Struct
{
    public struct FightResult
    {
        public Character Winner;
        public Character Looser;
        public EnumMode KillMode;
    }
}