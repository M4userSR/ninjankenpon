﻿using UnityEngine.InputSystem.PlayerInput;
using Photon.Pun;
using NinJanKenPon.Struct;

namespace NinJanKenPon
{
    public class AvatarLobby : MonoBehaviourPun
    {
        int OwnerId = -1; // Owner from network, PhotonNetwork.LocalPlayer.ActorNumber == -1 when offline or outside a room

        public delegate void AvatarLobbyDelegate();
        public AvatarLobbyDelegate OnChoiceSubmit;
        public AvatarLobbyDelegate OnChoiceCancel;
        public AvatarLobbyDelegate OnChoiceNext;
        public AvatarLobbyDelegate OnChoicePrevious;
        public bool ReadInput { get => !PhotonNetwork.IsConnected || PhotonNetwork.LocalPlayer.ActorNumber == OwnerId; }

        void OnSubmit()
        {
            if (!ReadInput)
                return;

            if (null != OnChoiceSubmit)
                OnChoiceSubmit();
        }

        void OnCancel()
        {
            if (!ReadInput)
                return;

            if (null != OnChoiceCancel)
                OnChoiceCancel();
        }

        void OnAvatarChoice(InputValue value)
        {
            if (!ReadInput)
                return;

            float inputValue = value.Get<float>();

            if (inputValue == 1 && null != OnChoiceNext)
                OnChoiceNext();
            if (inputValue == -1 && null != OnChoicePrevious)
                OnChoicePrevious();
        }

        public void Initialize(ControllerPlayer cp)
        {
            if (null != cp.PlayerNetwork)
                OwnerId = cp.PlayerNetwork.ActorNumber;
        }
    }
}