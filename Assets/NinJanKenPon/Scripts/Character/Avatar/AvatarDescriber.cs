﻿using UnityEngine;

namespace NinJanKenPon.AvatarSystem
{
    [CreateAssetMenu(fileName = "NewAvatar", menuName = "NinJanKenPon/Avatar")]
    public class AvatarDescriber : ScriptableObject
    {
        [Header("Representation")]
        [Tooltip ("Default sprite to represent the avatar in pixel art")]
        public Sprite Sprite;
        public GameObject Prefab;
    }
}
