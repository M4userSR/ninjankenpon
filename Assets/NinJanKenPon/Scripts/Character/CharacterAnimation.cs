﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NinJanKenPon.Animation;
using NinJanKenPon.Enum;

namespace NinJanKenPon
{
    [RequireComponent(typeof(Animator))]
    public class CharacterAnimation : MonoBehaviour
    {
        [Tooltip("Folder which contain all the animation of the avatar (in Animations/Resources/Character)")]
        public string AvatarAnimationFolderName = null;
        public string CharacterAnimationFolderPath { get => string.Format("Character/{0}", AvatarAnimationFolderName); }
        [Tooltip("Folder list which contain dynamic animations per mode of the avatar")]
        public string[] PerModeAnimationFolderList;
        [HideInInspector] public EnumMode CurrentMode;

        Animator Animator;
        AnimationClipOverrides ClipOverrides;
        AnimatorOverrideController AnimatorOverrideController;
        Object[] AnimationClipObjectList;
        Dictionary<EnumMode, Object[]> PerModeAnimationDictionary = new Dictionary<EnumMode, Object[]>();

        void Awake()
        {
            Animator = GetComponent<Animator>();
            AnimatorOverrideController = new AnimatorOverrideController(Animator.runtimeAnimatorController);
            Animator.runtimeAnimatorController = AnimatorOverrideController;

            if (null == AvatarAnimationFolderName)
                Debug.LogErrorFormat("[CharacterAnimation] AvatarAnimationFolderName is missing for character {0}", name);
        }

        void Start()
        {
            ClipOverrides = new AnimationClipOverrides(AnimatorOverrideController.overridesCount);
            AnimatorOverrideController.GetOverrides(ClipOverrides);

            LoadAllAnimation();
            UpdateAllAnimation();
        }

        public void LoadAllAnimation()
        {
            if (null == AvatarAnimationFolderName)
                return;

            AnimationClipObjectList = Resources.LoadAll(CharacterAnimationFolderPath);

            List<EnumMode> modeList = EnumMode.GetValues(typeof(EnumMode)).Cast<EnumMode>().ToList();
            foreach (EnumMode mode in modeList)
                foreach (string folder in PerModeAnimationFolderList)
                {
                    if (!PerModeAnimationDictionary.ContainsKey(mode))
                        PerModeAnimationDictionary.Add(mode, new Object[0]);

                    PerModeAnimationDictionary[mode] = PerModeAnimationDictionary[mode].Concat(Resources.LoadAll(string.Format("{0}/{1}/{2}", folder, AvatarAnimationFolderName, mode.ToString()))).ToArray();
                }
        }

        public void UpdateAllAnimation()
        {
            foreach (AnimationClip clipObject in AnimationClipObjectList)
                ClipOverrides[clipObject.name] = clipObject;
            
            UpdatePerModeAnimation();
        }

        public void UpdatePerModeAnimation()
        {
            if (!PerModeAnimationDictionary.ContainsKey(CurrentMode))
                return;

            Object[] clipObjectList = PerModeAnimationDictionary[CurrentMode];

            foreach (AnimationClip clipObject in clipObjectList)
                ClipOverrides[clipObject.name] = clipObject;

            AnimatorOverrideController.ApplyOverrides(ClipOverrides);
        }
    }
}
