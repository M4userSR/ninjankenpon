﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NinJanKenPon.Enum;
using NinJanKenPon.Struct;
using NinJanKenPon.StateSystem;
using NinJanKenPon.MovementState;
using NinJanKenPon.MindState;
using NinJanKenPon.WeaponSystem;

namespace NinJanKenPon
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(CharacterMode))]
    public class Character : MonoBehaviour
    {
        [Header("General")]
        [Tooltip("The fastest the player can travel in the x axis")]
        public float MaxSpeed = 10f;
        [Tooltip("The limit of velocity the player can reach")]
        public float MaxVelocity = 150f;
        public SpriteRenderer Avatar;

        [Header("Checkers")]
        [Tooltip("Radius of the overlap circle to determine if grounded")]
        public float GroundedRadius = .1f;
        [Tooltip("Radius of the overlap circle to determine if the player can stand up")]
        public float CeilingRadius = .1f;
        [Tooltip("Radius of the overlap circle to determine if walled")]
        public float WalledRadius = .1f;
        [Tooltip("A position marking where to check for ceilings")]
        public Transform CeilingCheck;
        [Tooltip("A position marking where to check if the player is grounded")]
        public Transform GroundCheck;
        [Tooltip("A position marking where to check if the player is walled")]
        public Transform WallCheck;

        [Header("Layers masking")]
        [Tooltip("A mask determining what is ground to the character")]
        public LayerMask WhatIsGround;
        [Tooltip("A mask determining what is wall to the character")]
        public LayerMask WhatIsWall;
        
        [Header("Wall")]
        [Range(0, 1)] [Tooltip("Maximum amount of drag applied to wall velocity. 1 = 100%")]
        public float MaxWallDrag = 0f;
        [Range(0, 1)] [Tooltip("Minimum amount of drag applied to wall velocity. 1 = 100%")]
        public float MinWallDrag = 0f;
        [Range(0, 1)] [Tooltip("When the player can get off from a wall. Values corresponding to the x axis value. 0 = Instant, 1 = maximum X axis")]
        public float GetOffWallLimit = 0f;

        [Header("Air")]
        [Tooltip("Multiplier on the current velocity when falling. 0 = default behavior")]
        public float AirFallMultiplier = 0f;

        [Header("Jump")]
        [Tooltip("The maximum of jumps that the player can make")]
        public int MaxJump = 2;
        [Tooltip("Amount of force added when the player jumps")]
        public float JumpForce = 400f;
        [Tooltip("Multiplier on the current velocity to slow down the air velocity when it's a small jump. 0 = default behavior")]
        public float LowJumpMultiplier = 0f;

        [Header("Jump from a wall")]
        [Tooltip("Smooth the force added when a players jumps from a wall. x = time, y = force")]
        public AnimationCurve WallJumpForceAc;
        [Tooltip("Smooth the force added when a players jumps from a wall in the opposite direction. x = time, y = force")]
        public AnimationCurve WallJumpForceOppositeAc;
        [Range(0, 1)] [Tooltip("When the player can controls the character during a jump from a wall. 0 = At the beginning (0%), 1 = At the end of the jump (100%)")]
        public float WallJumpNoControlLimit = 1f;
        [Range(0, 1)] [Tooltip("When the system will check facing during a jump from a wall. 0 = At the beginning (0%), 1 = At the end of the jump (100%)")]
        public float WallJumpFacingCheckLimit = 1f;
        [Tooltip("Speed up or slow down the horizontal velocity when the player jumps from a wall according to the direction of horizontal axis")]
        public float WallJumpMultiplier = 0f;
        [Tooltip("Delay since the previous jump's button pressed which can be considered as a wall jump wanted by the player")]
        public float DelayEarlyJump = 0f;

        [Header("Stun")]
        public float DelayStunned = 0f;
        public float StunPushForce = 0f;

        [Header("Strike")]
        [Tooltip("Delay before the player can strike again")]
        public float DelayBeforeStrike = 0f;
        public float SpecialStrikeTreshold = 0.1f;
        public SpecialStrikeMatrixDescriber SpecialStrikeMatrix;

        [Header("Mind State")]
        public List<MindStateScoring> ScorePerMindState = new List<MindStateScoring>();
        public List<MindStateColor> ColorPerMindState = new List<MindStateColor>();

        [Header("Debug")]
        [SerializeField]
        int NumberOfFramesPositionPrediction = 10;
        [SerializeField]
        float PositionPredictionRadius = 0.3f;


        public bool DoJump { get; private set; } = false; // Jump is asked
        public bool HoldJump { get; private set; } = false; // Jump asked is continuous holded
        public bool FacingRight { get; private set; } = true; // For determining which way the player is currently facing
        [HideInInspector] public bool AirControl = false; // Whether or not a player can move while jumping
        [HideInInspector] public bool AutoCheckMovementState = true;
        public Vector2 FacingDirection { get => FacingRight ? Vector2.right : Vector2.left; }
        public Vector2 FacingDirectionInverse { get => FacingRight ? Vector2.left : Vector2.right; }
        public IEnumerator WallJumpCoroutine { get; private set; }
        public BaseMovementState CurrentMovementState { get => (BaseMovementState)MovementStateManager.CurrentState; set => MovementStateManager.CurrentState = value; }
        public BaseMindState CurrentMindState { get => (BaseMindState)MindStateManager.CurrentState; set => MindStateManager.CurrentState = value; }
        [HideInInspector] public EnumJump JumpType = EnumJump.Classic; // Type of Jump
        public WeaponHitbox WeaponHitbox { get => CharacterMode.WeaponHitbox; }


        public delegate void PlayerDelegate(Transform player);
        public static PlayerDelegate OnDie;


        int JumpCount = 0; // Current number of jump made during a jump state.
        int StrikeDirection = 0; // 1 = up, -1 = down, 0 = left/right
        float TimeSincePreviousJump = 0f; // Timer
        float TimeSincePreviousStrike = 0f; // Timer
        float StrikeChargeValue = 0f; // Value of time holding the strike button
        bool CheckFacing = true; // If not CheckFacing, Flip() will be locked
        bool IsWallJumpInOppositeDirection = false; // The player want to wall jump from the wall to the wall
        bool CanJump { get => JumpCount < MaxJump; }
        bool CanStrike { get => TimeSincePreviousStrike >= DelayBeforeStrike; }
        bool ChargingStrike = false; // Player is holding strike button
        bool CanBeSpecialStrike = false; // Player is holding a combination of a special strike in the current mode (See SpecialStrikeMatrix SO)
        bool IsSpecialStrike = false; // Player's intention is to release a special strike
        Rigidbody2D Rigidbody2D;
        Animator Animator;
        CharacterMode CharacterMode;
        StateManager<Character> MovementStateManager;
        StateManager<Character> MindStateManager;
        Vector2 MoveDirection; // Current Move direction from axis each frame
        Vector2 MoveAmount { get => MoveDirection * MaxSpeed; }

        void Awake()
        {
            // Setting up references
            Animator = GetComponent<Animator>();
            Rigidbody2D = GetComponent<Rigidbody2D>();
            CharacterMode = GetComponent<CharacterMode>();
            MovementStateManager = new StateManager<Character>();
            MindStateManager = new StateManager<Character>();
            // Default State
            CurrentMovementState = new GroundState(this);
            CurrentMindState = new NormalState(this);
            // Other stuffs
            TimeSincePreviousStrike = DelayBeforeStrike;
        }

        void FixedUpdate()
        {
            TimeSincePreviousJump += Time.fixedDeltaTime;
            TimeSincePreviousStrike += Time.fixedDeltaTime;

            if (ChargingStrike)
                StrikeChargeValue += Time.fixedDeltaTime;
            else
                StrikeChargeValue = 0;

            Animator.SetFloat("SpeedX", Mathf.Abs(Rigidbody2D.velocity.x));
            Animator.SetFloat("SpeedY", Mathf.Abs(Rigidbody2D.velocity.y));
            Animator.SetFloat("VelocityX", Rigidbody2D.velocity.x);
            Animator.SetFloat("VelocityY", Rigidbody2D.velocity.y);
            Animator.SetFloat("RatioMaxSpeed", Mathf.Abs(MoveAmount.x) / MaxSpeed);
            Animator.SetFloat("RatioMaxVelocityX", Mathf.Abs(Rigidbody2D.velocity.x) / MaxVelocity);
            Animator.SetFloat("RatioMaxVelocityY", Mathf.Abs(Rigidbody2D.velocity.y) / MaxVelocity);
            Animator.SetFloat("StrikeDirection", StrikeDirection);
            Animator.SetInteger("JumpCount", JumpCount);
            Animator.SetBool("Grounded", CurrentMovementState is GroundState);
            Animator.SetBool("Walled", CurrentMovementState is WallState);
            Animator.SetBool("Stunned", CurrentMovementState is StunState);
            Animator.SetBool("Striking", !CanStrike);
            Animator.SetBool("SpecialStrike", IsSpecialStrike);

            CheckVelocity();
            CheckSpecialStrike();

            if (AutoCheckMovementState)
                SetMovementState();

            CurrentMovementState.Tick();
        }

        void OnDrawGizmos()
        {
            if (null == MovementStateManager)
                return;

            // Debug GroundCheck
            Gizmos.color = CurrentMovementState is GroundState ? Color.green : Color.red;
            Gizmos.DrawSphere(GroundCheck.position, GroundedRadius);
            // Debug CeilingCheck
            Gizmos.color = Color.grey;
            Gizmos.DrawSphere(CeilingCheck.position, CeilingRadius);
            // Debug WallCheck
            Gizmos.color = CurrentMovementState is WallState ? Color.green : Color.red;
            Gizmos.DrawSphere(WallCheck.position, WalledRadius);
            // Debug Input direction
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, transform.position + (Vector3)MoveDirection);
            // Debug Predicted next direction
            Gizmos.color = Color.yellow;
            for (int i = NumberOfFramesPositionPrediction; i > 0; i -= 2)
                Gizmos.DrawSphere(PredicatePosition(i), PositionPredictionRadius);
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (CurrentMovementState is BaseSpecialStrikeState)
            {
                ResetSpecialStrike();
                SetMovementState();
            }
        }

        void SetMovementState()
        {
            if (!CheckForGround())
                if (!CheckForWall() && !(CurrentMovementState is AirState))
                    CurrentMovementState = new AirState(this);
        }

        void CheckVelocity()
        {
            Vector2 velocity = Rigidbody2D.velocity;
            velocity.x = Mathf.Clamp(velocity.x, MaxVelocity * -1, MaxVelocity);
            velocity.y = Mathf.Clamp(velocity.y, MaxVelocity * -1, MaxVelocity);
            Rigidbody2D.velocity = velocity;
        }

        void CheckSpecialStrike()
        {
            if (!ChargingStrike || !CanBeSpecialStrike || StrikeChargeValue < SpecialStrikeTreshold)
                return;

            if (!IsSpecialStrike) // /!\ This instruction must be before reseting strike CD
                SpecialStrike();

            TimeSincePreviousStrike = 0f; // Reset strike CD while special strike was not released
        }

        void ImpulseJump()
        {
            Rigidbody2D.velocity = Vector2.zero;
            WallJumpCoroutine = HorizontalImpulsion();
            StartCoroutine(WallJumpCoroutine); // Add force to a horizontal direction over time
            Rigidbody2D.AddForce(Vector2.up * JumpForce); // Then add force to the upper direction
        }
        
        void VerticalJump()
        {
            Rigidbody2D.velocity = new Vector2(Rigidbody2D.velocity.x, 0);
            Rigidbody2D.AddForce(Vector2.up * JumpForce);
        }

        void SpecialStrike()
        {
            if (!CanStrike || IsSpecialStrike)
                return;

            if (CurrentMovementState is AirState)
                CurrentMovementState = new AirStrikeState(this);
            else if (CurrentMovementState is GroundState)
                CurrentMovementState = new GroundStrikeState(this);
            else
                return;

            IsSpecialStrike = true;
            Animator.SetBool("SpecialStrike", IsSpecialStrike);
            Animator.SetTrigger("Strike");
        }

        void ResetSpecialStrike()
        {
            CanBeSpecialStrike = false;
            IsSpecialStrike = false;
        }

        void ReleaseSpecialStrike()
        {

        }

        bool CheckForGround()
        {
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead
            Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, GroundedRadius, WhatIsGround);

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject == gameObject)
                    continue;
                    
                if (!(CurrentMovementState is GroundState))
                    CurrentMovementState = new GroundState(this);

                return true;
            }

            return false;
        }

        bool CheckForWall()
        {
            // The player is walled if a circlecast to the wallcheck position hits anything designated as a wall
            // This can be done using layers instead
            Collider2D[] colliders = Physics2D.OverlapCircleAll(WallCheck.position, WalledRadius, WhatIsWall);

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject == gameObject)
                    continue;

                if (!(CurrentMovementState is WallState))
                    CurrentMovementState = new WallState(this, colliders[i].ClosestPoint(transform.position));

                return true;
            }

            return false;
        }

        Vector3 PredicatePosition(int framePosition)
        {
            Vector3 pos = transform.position;
            Vector3 velocity = (Vector3)Rigidbody2D.velocity;
            return pos += velocity * Time.fixedDeltaTime * framePosition;
        }

        IEnumerator HorizontalImpulsion()
        {
            IsWallJumpInOppositeDirection = MoveAmount.x > 0 && !FacingRight || MoveAmount.x < 0 && FacingRight;
            AnimationCurve ac = IsWallJumpInOppositeDirection ? WallJumpForceOppositeAc : WallJumpForceAc;

            if (ac.length == 0)
                yield break;

            Keyframe lastKey = ac.keys[ac.length-1];
            Vector2 direction = FacingDirection;
            float totalTime = lastKey.time;
            float timer = lastKey.time;
            float progress = 0f;

            AirControl = false;
            CheckFacing = false;

            while (timer >= 0)
            {
                Rigidbody2D.velocity = new Vector2(0, Rigidbody2D.velocity.y);
                float force = ac.Evaluate(timer);
                Rigidbody2D.AddForce(direction * force);
                    
                timer -= Time.fixedDeltaTime;
                progress = 1 - timer / totalTime;

                if (!AirControl && !IsWallJumpInOppositeDirection && progress >= WallJumpNoControlLimit)
                    AirControl = true;

                if (!CheckFacing && (IsWallJumpInOppositeDirection || progress >= WallJumpFacingCheckLimit))
                    CheckFacing = true;

                yield return new WaitForFixedUpdate();
            }

            AirControl = true;
            CheckFacing = true;
            IsWallJumpInOppositeDirection = false;
        }

        IEnumerator StopStun()
        {
            yield return new WaitForSeconds(DelayStunned);
            SetMovementState();
        }

        public void StopWallJump()
        {
            if (null == WallJumpCoroutine)
                return;

            StopCoroutine(WallJumpCoroutine);
            WallJumpCoroutine = null;

            AirControl = true;
            CheckFacing = true;
            IsWallJumpInOppositeDirection = false;
        }

        public void Flip()
        {
            if (!CheckFacing)
                return;

            FacingRight = !FacingRight;
            transform.Rotate(0f, 180f, 0f);
        }

        public void FlipFromMove()
        {
            if (MoveAmount.x > 0 && !FacingRight || MoveAmount.x < 0 && FacingRight)
                Flip();
        }

        public void ResetJump()
        {
            JumpCount = 0;
        }

        public bool IsPreviousJumpEarly()
        {
            return TimeSincePreviousJump <= DelayEarlyJump;
        }

        public void Jump()
        {
            if (!CanJump)
                return;

            TimeSincePreviousJump = 0f;
            JumpCount++;
            
            if (JumpType == EnumJump.Wall) // WallJump can be only executed once on multiple jump
            {
                ImpulseJump();
                JumpType = EnumJump.Classic;
                return;
            }

            VerticalJump();
        }

        public void DefaultHorizontalMove()
        {
            if (null != WallJumpCoroutine && MoveAmount.x != 0) // TODO : Pas mieux à faire ?
                StopWallJump();

            Rigidbody2D.velocity = new Vector2(MoveAmount.x, Rigidbody2D.velocity.y);
            FlipFromMove();
        }

        public void HorizontalWallJumpVelocity()
        {
            float x = MoveDirection.x;

            if (x == 0)
                return;

            FlipFromMove();

            if (IsWallJumpInOppositeDirection && HoldJump)
                return;

            Vector2 direction = x >= 0 ? Vector2.right : Vector2.left;
            Rigidbody2D.velocity += direction * WallJumpMultiplier;
        }

        public void VerticalAirVelocity()
        {
            if (Rigidbody2D.velocity.y < 0)
                Rigidbody2D.velocity += Vector2.down * AirFallMultiplier; // Faster fall

            if (Rigidbody2D.velocity.y > 0 && !HoldJump)
                Rigidbody2D.velocity += Vector2.down * LowJumpMultiplier; // Lower jump
        }

        public void WallHorizontalMove()
        {
            bool getOffFromLeft = FacingRight && MoveDirection.x >= GetOffWallLimit;
            bool getOffFromRight = !FacingRight && MoveDirection.x <= -1 * GetOffWallLimit;
            float horizontalMove = getOffFromLeft || getOffFromRight ? MoveAmount.x : 0;
            float verticalDirection = MoveDirection.y < 0 ? -1 * MoveDirection.y : 0;
            float dragAmount = 1 - Mathf.Clamp(1 - verticalDirection, MinWallDrag, MaxWallDrag);

            Rigidbody2D.velocity = new Vector2(horizontalMove, Rigidbody2D.velocity.y * dragAmount);
        }

        public void Move(Vector2 moveInput, bool doJump, bool holdJump)
        {
            MoveDirection = moveInput;
            DoJump = doJump;
            HoldJump = holdJump;
        }

        public void Push(Vector2 direction, float force) // Be careful that the rigidbody velocity is not distorted at the next frame when using this
        {
            if (FacingDirection == direction)
                Flip();

            Rigidbody2D.AddForce(direction * force);
        }

        public void Stun(Vector2? pushDirection)
        {
            Rigidbody2D.velocity = Vector2.zero;

            if (pushDirection.HasValue && pushDirection.Value.y == 0)
                pushDirection = new Vector2(pushDirection.Value.x, 1);

            CurrentMovementState = new StunState(this, pushDirection);

            StartCoroutine(StopStun());
        }

        public void NextWeapon()
        {
            CharacterMode.NextMode();
        }

        public void PreviousWeapon()
        {
            CharacterMode.PreviousMode();
        }

        public void SetPaletteColor(int index)
        {
            Avatar.material.SetInt("Vector1_356F492F", index);
        }

        public void ChargeStrike()
        {
            ChargingStrike = true;

            StrikeDirection = 0;
            if (MoveDirection.y > 0)
                StrikeDirection = 1;
            else if (MoveDirection.y < 0)
                StrikeDirection = -1;

            if (null != SpecialStrikeMatrix && SpecialStrikeMatrix.Contain(CurrentMovementState.Type, CharacterMode.ActiveMode.Type, StrikeDirection))
                CanBeSpecialStrike = true;
        }

        public void Strike()
        {
            if (!CanStrike && !IsSpecialStrike) // TODO : Redéfinir la notion de CanStrike ? Dans le cas d'une atq spé, CanStrike est faux puisque cela est basé sur le CD du strike qui est déjà reset dans ce cas précis (voir CheckSpecialStrike())
                return;

            TimeSincePreviousStrike = 0f;
            ChargingStrike = false;

            if (IsSpecialStrike)
            {
                ReleaseSpecialStrike();
                return;
            }

            Animator.SetTrigger("Strike");
        }

        public void Die()
        {
            if (null != OnDie)
                OnDie(transform);
        }
    }
}

