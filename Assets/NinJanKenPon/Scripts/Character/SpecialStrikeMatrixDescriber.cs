﻿using System.Collections.Generic;
using UnityEngine;
using NinJanKenPon.Enum;
using NinJanKenPon.Struct;

namespace NinJanKenPon
{
    [CreateAssetMenu(fileName = "NewSpecialStrikeMatrix", menuName = "NinJanKenPon/Special strike matrix")]
    public class SpecialStrikeMatrixDescriber : ScriptableObject
    {
        public List<MovementStateSpecialStrike> Matrix;

        public bool Contain(EnumMovementState movementState, EnumMode mode, int strikeDirection)
        {
            System.Predicate<MovementStateSpecialStrike> predicate = el => el.MovementState == movementState;
            int index = Matrix.FindIndex(predicate);

            if (index == -1)
                return false;

            MovementStateSpecialStrike matrixValue = Matrix.Find(predicate);

            System.Predicate<ModeDirection> predicateMode = el => el.Mode == mode;
            index = matrixValue.ModeDirectionList.FindIndex(predicateMode);

            if (index == -1)
                return false;

            ModeDirection modeDirection = matrixValue.ModeDirectionList.Find(predicateMode);

            return modeDirection.DirectionAvailable.Count == 0 || modeDirection.DirectionAvailable.Contains(strikeDirection);
        }
    }
}
