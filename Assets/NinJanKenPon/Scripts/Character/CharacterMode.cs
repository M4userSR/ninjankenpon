﻿using UnityEngine;
using NinJanKenPon.ModeSystem;
using NinJanKenPon.WeaponSystem;
using NinJanKenPon.Mecanics.GateSystem;

namespace NinJanKenPon
{
    [RequireComponent(typeof(CharacterAnimation))]
    public class CharacterMode : MonoBehaviour
    {
        [Header("General")]
        [Tooltip("Delay before applying the new mode")]
        [SerializeField] float DelayBeforeApplyMode = 1f;
        [Tooltip("Cooldown before be able to change mode again")]
        [SerializeField] float CooldownChangingMode = 1f;
        [Tooltip("List of mode available of the character")]
        [SerializeField] ModeDescriber[] ModeList = null;

        [Header("Sign representing the current mode")]
        [Tooltip("Prefab of the GameObject representig the sign of a mode")]
        [SerializeField] GameObject SignModePrefab = null;
        [Tooltip("Transform sign will follow")]
        [SerializeField] Transform SignModeTarget = null;

        [Header("Attack system")]
        [Tooltip("Script that handle the weapon hitbox")]
        public WeaponHitbox WeaponHitbox = null;

        int SelectedMode = 0;
        int? WaitingMode = null; // Next mode waiting to be apply
        float TimeSinceChangeMode = 0f; // Timer
        SignMode SignMode;
        CharacterAnimation CharacterAnimation;

        public ModeDescriber ActiveMode
        {
            get {
                return ModeList[SelectedMode];
            }
        }

        void Awake()
        {
            SubscribeEvents();

            CharacterAnimation = GetComponent<CharacterAnimation>();
            TimeSinceChangeMode = CooldownChangingMode;
            WeaponHitbox.Owner = gameObject;
            WeaponHitbox.CurrentMode = ActiveMode;
            CharacterAnimation.CurrentMode = ActiveMode.Type;

        }

        void Start()
        {
            SpawnModeSign();
            UpdateSign();
        }

        void SubscribeEvents()
        {
            GateManager.OnTeleport += OnObjectTeleported;
        }

        void FixedUpdate()
        {
            TimeSinceChangeMode += Time.fixedDeltaTime;
        }

        void OnEnable()
        {
            if (null != SignMode)
                SignMode.gameObject.SetActive(true);
        }

        void OnDisable()
        {
            if (null != SignMode)
                SignMode.gameObject.SetActive(false);
        }

        void SpawnModeSign()
        {
            GameObject signModeGo = Instantiate(SignModePrefab, transform.position, Quaternion.identity, transform.parent);
            signModeGo.name = "SignMode";

            SignMode = signModeGo.GetComponent<SignMode>();
            SignMode.Target = SignModeTarget;
            SignMode.OnUnload += ApplyNewMode;
        }

        void UpdateSign()
        {
            if (null == SignMode)
                return;

            SignMode.UpdateSign(ActiveMode);
        }

        void SwitchMode(int index)
        {
            if (!CanSwitchMode())
                return;

            if (Debug.isDebugBuild)
                Debug.Log("[CharacterMode] A mode is waiting to change");

            WaitingMode = index;

            SignMode.Unload(DelayBeforeApplyMode);
        }

        void ApplyNewMode()
        {
            if (!WaitingMode.HasValue)
                return;

            string oldModeName = ActiveMode.Type.ToString();

            SelectedMode = WaitingMode.Value;
            WeaponHitbox.CurrentMode = ActiveMode;
            WaitingMode = null;

            if (Debug.isDebugBuild)
                Debug.LogFormat("[CharacterMode] Player#{0} changed mode from {1} to {2}", name, oldModeName, ActiveMode.Type.ToString());

            CharacterAnimation.CurrentMode = ActiveMode.Type;
            CharacterAnimation.UpdatePerModeAnimation();
            UpdateSign();

            SignMode.LoadSign(CooldownChangingMode);
            TimeSinceChangeMode = 0f;
        }

        void OnObjectTeleported(Transform objectTeleported)
        {
            // TODO trouver autre chose pour le signmode qui suit le personnage lors de la téléportation
            if (null == gameObject || null == objectTeleported || null == objectTeleported.gameObject || objectTeleported.gameObject != gameObject)
                return;

            SignMode.ResetToPosition();
        }

        bool CanSwitchMode()
        {
            return TimeSinceChangeMode > CooldownChangingMode;
        }

        public void NextMode()
        {
            SwitchMode(SelectedMode == ModeList.Length - 1 ? 0 : SelectedMode + 1);
        }

        public void PreviousMode()
        {
            SwitchMode(SelectedMode == 0 ? ModeList.Length - 1 : SelectedMode - 1);
        }
    }
}
