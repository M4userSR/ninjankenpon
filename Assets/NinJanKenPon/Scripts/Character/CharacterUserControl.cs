﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.InputSystem.PlayerInput;
using Photon.Pun;

namespace NinJanKenPon
{
    [RequireComponent(typeof(Character))]
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PhotonView))]
    public class CharacterUserControl : MonoBehaviour
    {
        bool Assigned
        {
            get { return Id != -1; }
        }
        bool JumpClicked = false;
        bool JumpHold = false;
        Vector2 MoveVector = Vector2.zero;
        PlayerInput PlayerInput;
        PhotonView PhotonView;

        public int Id = -1;
        public Character Character {
            get; private set;
        }

        void Awake()
        {
            // Setting up references
            Character = GetComponent<Character>();
            PlayerInput = GetComponent<PlayerInput>();
            PhotonView = GetComponent<PhotonView>();
        }

        void OnEnable()
        {
            PlayerInput.enabled = true;
            PhotonView.enabled = PhotonNetwork.IsConnected;
        }

        void OnDisable()
        {
            PlayerInput.enabled = false;
            PhotonView.enabled = false;
        }

        void FixedUpdate()
        {
            if (!Assigned)
                return;

            Character.Move(MoveVector, JumpClicked, JumpHold);

            JumpClicked = false;
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            MoveVector = context.ReadValue<Vector2>();
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            if (context.interaction is PressInteraction)
                JumpClicked = true;
            else if (context.interaction is HoldInteraction)
            {
                if (context.started)
                    JumpHold = true;
                else
                    JumpHold = false;
            }
        }

        public void OnStrike(InputAction.CallbackContext context)
        {
            if (!Assigned)
                return;

            if (context.started)
                Character.ChargeStrike();
            else
                Character.Strike();
        }

        public void OnNextWeapon(InputAction.CallbackContext context)
        {
            if (!Assigned)
                return;

            Character.NextWeapon();
        }

        public void OnPreviousWeapon(InputAction.CallbackContext context)
        {
            if (!Assigned)
                return;

            Character.PreviousWeapon();
        }
    }
}

