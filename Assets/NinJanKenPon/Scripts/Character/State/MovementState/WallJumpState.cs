﻿using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public class WallJumpState : ClassicJumpState
    {
        public WallJumpState(Character mono, bool jump = false) : base(mono, jump) {}

        protected override void JumpCheck()
        {
            if (Owner.DoJump)
                Owner.AirControl = true;

            base.JumpCheck();
        }

        public override void OnStateEnter()
        {
            Owner.JumpType = EnumJump.Wall;
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            Owner.StopWallJump();
            base.OnStateExit();
        }

        public override void Tick()
        {
            base.Tick();

            if (!Owner.AirControl)
                Owner.HorizontalWallJumpVelocity();
        }
    }
}