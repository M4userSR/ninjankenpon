﻿using NinJanKenPon.StateSystem;

using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public class GroundState : BaseMovementState
    {
        public GroundState(Character mono) : base(mono) {}

        public override EnumMovementState Type { get => EnumMovementState.Ground; }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick()
        {
            if (Owner.DoJump)
            {
                Owner.CurrentMovementState = new ClassicJumpState(Owner, true);
                return;
            }

            Owner.DefaultHorizontalMove();
        }
    }
}
