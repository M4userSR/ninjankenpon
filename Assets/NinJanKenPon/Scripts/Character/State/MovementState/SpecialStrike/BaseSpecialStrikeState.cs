﻿using UnityEngine;

namespace NinJanKenPon.MovementState
{
    public abstract class BaseSpecialStrikeState : BaseMovementState
    {
        protected Animator Animator;
        protected Rigidbody2D Rigidbody2D;

        public BaseSpecialStrikeState(Character mono) : base(mono) {
            Animator = Owner.GetComponent<Animator>();
            Rigidbody2D = Owner.GetComponent<Rigidbody2D>();
        }

        public override void OnStateEnter()
        {
            Owner.AirControl = false;
            Owner.AutoCheckMovementState = false;
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            Owner.AirControl = true;
            Owner.AutoCheckMovementState = true;
            base.OnStateExit();
        }
    }
}