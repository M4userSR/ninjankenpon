﻿using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public class AirStrikeState : BaseSpecialStrikeState
    {
        public AirStrikeState(Character mono) : base(mono) {}

        public override EnumMovementState Type => EnumMovementState.Air;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick()
        {
            
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}