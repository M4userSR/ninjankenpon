﻿using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public class GroundStrikeState : BaseSpecialStrikeState
    {
        public GroundStrikeState(Character mono) : base(mono) {}

        public override EnumMovementState Type => EnumMovementState.Ground;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick()
        {

        }
    }
}