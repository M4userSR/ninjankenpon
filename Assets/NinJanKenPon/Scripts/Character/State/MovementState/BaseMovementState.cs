using NinJanKenPon.StateSystem;
using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public abstract class BaseMovementState : State<Character>
    {
        public BaseMovementState(Character mono) : base(mono) {}

        public abstract EnumMovementState Type { get; }
    }
}