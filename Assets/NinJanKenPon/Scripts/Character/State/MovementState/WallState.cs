﻿using UnityEngine;
using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public class WallState : BaseMovementState
    {
        Vector2 ClosestPoint;
        Vector2 NormalizedDirection { get => (ClosestPoint - (Vector2)Owner.transform.position).normalized; }

        public override EnumMovementState Type { get => EnumMovementState.Wall; }

        public WallState(Character mono, Vector2 closestPointFromWall) : base(mono) {
            ClosestPoint = closestPointFromWall;
        }

        void ToWallJump()
        {
            Owner.CurrentMovementState = new WallJumpState(Owner, true);
        }

        public override void OnStateEnter()
        {
            if (Owner.FacingDirection.x == Mathf.Round(NormalizedDirection.x))
                Owner.Flip();

            base.OnStateEnter();

            if (Owner.IsPreviousJumpEarly() && null != PreviousState && PreviousState.GetType() == typeof(ClassicJumpState))
                ToWallJump();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick()
        {
            if (Owner.DoJump)
            {
                ToWallJump();
                return;
            }

            Owner.WallHorizontalMove();
        }
    }
}