﻿using NinJanKenPon.Enum;
using NinJanKenPon.Mecanics;
using UnityEngine;

namespace NinJanKenPon.MovementState
{
    public class StunState : BaseMovementState
    {
        Vector2? PushDirection;

        public override EnumMovementState Type { get => EnumMovementState.Stun; }

        public StunState(Character mono, Vector2? pushDirection) : base(mono) {
            PushDirection = pushDirection;
        }

        public override void OnStateEnter()
        {
            Owner.AutoCheckMovementState = false;

            base.OnStateEnter();

            if (PushDirection.HasValue)
            {
                Owner.Push(PushDirection.Value, Owner.StunPushForce);
                Owner.Avatar.GetComponent<Shakable>().Shake(Owner.DelayStunned);
            }
        }

        public override void OnStateExit()
        {
            Owner.AutoCheckMovementState = true;
            base.OnStateExit();
        }

        public override void Tick() {}
    }
}
