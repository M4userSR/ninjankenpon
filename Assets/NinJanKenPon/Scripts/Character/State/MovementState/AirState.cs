﻿using NinJanKenPon.Enum;

namespace NinJanKenPon.MovementState
{
    public class AirState : BaseMovementState
    {
        protected bool initialJump = false;

        public override EnumMovementState Type { get => EnumMovementState.Air; }

        public AirState(Character mono, bool jump = false) : base(mono) {
            initialJump = jump;
        }

        protected virtual void JumpCheck()
        {
            if (Owner.DoJump)
                Owner.Jump();
        }

        protected virtual void MoveCheck()
        {
            if (Owner.AirControl)
                Owner.DefaultHorizontalMove();
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            if (initialJump)
                Owner.Jump();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
            Owner.ResetJump();
        }

        public override void Tick()
        {
            JumpCheck();
            MoveCheck();

            Owner.VerticalAirVelocity();
        }
    }
}