﻿namespace NinJanKenPon.MovementState
{
    public class ClassicJumpState : AirState
    {
        public ClassicJumpState(Character mono, bool jump = false) : base(mono, jump) {}
    }
}