using UnityEngine;

namespace NinJanKenPon.MindState
{
    public interface IMindState
    {
        int? GetScoreWhenTouch(Character touched); // This is positive score
        int? GetScoreWhenTouched(Character attacker); // This is negative score
    }
}