using NinJanKenPon.Enum;

namespace NinJanKenPon.MindState
{
    public class ShameState : BaseMindState
    {
        public ShameState(Character mono) : base(mono) {}

        public override EnumMindState Type { get { return EnumMindState.Shame; } }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick() {}
    }
}