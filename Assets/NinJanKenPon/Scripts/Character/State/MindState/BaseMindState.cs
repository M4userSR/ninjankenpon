using System.Collections.Generic;
using UnityEngine;
using NinJanKenPon.StateSystem;
using NinJanKenPon.Enum;
using NinJanKenPon.Struct;

namespace NinJanKenPon.MindState
{
    public abstract class BaseMindState : State<Character>, IMindState
    {
        public BaseMindState(Character mono) : base(mono) {}

        public abstract EnumMindState Type { get; }

        protected MindStateScoring? GetScoringByMindState(EnumMindState attackerMindState, EnumMindState touchedMindState) // TODO : Rendre générique dans un utils
        {
            int index = Owner.ScorePerMindState.FindIndex(element => element.AttackerState == attackerMindState && element.TouchedState == touchedMindState);

            if (index == -1)
                return null;

            return Owner.ScorePerMindState[index];
        }

        protected MindStateColor? GetColorOfMindState() // TODO : Rendre générique dans un utils
        {
            int index = Owner.ColorPerMindState.FindIndex(element => element.State == Type);

            if (index == -1)
                return null;

            return Owner.ColorPerMindState[index];
        }

        protected void ApplyMindStateColor()
        {
            MindStateColor? mindStateColor = GetColorOfMindState();
            Owner.Avatar.color = mindStateColor.HasValue ? mindStateColor.Value.Color : Color.white;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            ApplyMindStateColor();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick() {}

        public virtual int? GetScoreWhenTouch(Character touched)
        {
            MindStateScoring? scoring = GetScoringByMindState(Type, touched.CurrentMindState.Type);
            if (!scoring.HasValue)
                return null;

            return scoring.Value.ScoreGain;
        }

        public virtual int? GetScoreWhenTouched(Character attacker)
        {
            MindStateScoring? scoring = GetScoringByMindState(attacker.CurrentMindState.Type, Type);
            if (!scoring.HasValue)
                return null;

            return scoring.Value.ScoreLost;
        }
    }
}