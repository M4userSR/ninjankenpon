using NinJanKenPon.Enum;

namespace NinJanKenPon.MindState
{
    public class KillingSpreeState : BaseMindState
    {
        public KillingSpreeState(Character mono) : base(mono) {}

        public override EnumMindState Type { get { return EnumMindState.KillingSpree; } }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick() {}
    }
}