using NinJanKenPon.Enum;

namespace NinJanKenPon.MindState
{
    public class NormalState : BaseMindState
    {
        public NormalState(Character mono) : base(mono) {}

        public override EnumMindState Type { get { return EnumMindState.Normal; } }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
        }

        public override void Tick() {}
    }
}