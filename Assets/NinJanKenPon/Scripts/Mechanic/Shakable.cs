﻿using System.Collections;
using UnityEngine;

namespace NinJanKenPon.Mecanics
{
    public class Shakable : MonoBehaviour
    {
        public float magnitude = 1f;

        IEnumerator DoShake(float duration)
        {
            Vector3 originalPos = transform.localPosition;
            float elapsed = 0f;

            while (elapsed < duration)
            {
                transform.localPosition = Random.insideUnitCircle * Time.deltaTime * magnitude;
                elapsed += Time.deltaTime;

                yield return null;
            }

            transform.localPosition = originalPos;
        }

        public void Shake(float duration)
        {
            StartCoroutine(DoShake(duration));
        }
    }
}
