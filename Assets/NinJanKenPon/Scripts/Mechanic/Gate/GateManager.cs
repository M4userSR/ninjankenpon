﻿using UnityEngine;

namespace NinJanKenPon.Mecanics.GateSystem
{
    public class GateManager : MonoBehaviour
    {
        [SerializeField] Gate GateA = null;
        [SerializeField] Gate GateB = null;
        [SerializeField] Vector2 TeleportionFactor = Vector2.one;

        public delegate void GateManagerDelegate(Transform Object);
        public static GateManagerDelegate OnTeleport;

        void Awake()
        {
            if (null == GateA || null == GateB)
            {
                Debug.LogErrorFormat("[GateManager] A Gate script is missing on the gate manager {0}", name);
                return;
            }

            GateA.OnObjectInGate = GateATeleport;
            GateB.OnObjectInGate = GateBTeleport;
        }

        void GateATeleport(Transform transform)
        {
            TeleportTo(GateA, GateB, transform);
        }

        void GateBTeleport(Transform transform)
        {
            TeleportTo(GateB, GateA, transform);
        }

        void TeleportTo(Gate fromGate, Gate toGate, Transform transform)
        {
            if (Debug.isDebugBuild)
                Debug.LogFormat("[GateManager] Teleport {0} from gates {1}", transform.name, name);

            toGate.DisableTriggerStay = true;

            Vector3 positionInGate = fromGate.transform.InverseTransformPoint(transform.position) * TeleportionFactor;
            Vector3 toPosition = toGate.transform.TransformPoint(positionInGate);

            transform.position = toPosition;

            if (null != OnTeleport)
                OnTeleport(transform);
        }
    }
}
