﻿using UnityEngine;
using NinJanKenPon.Constant;

namespace NinJanKenPon.Mecanics.GateSystem
{
    [RequireComponent(typeof(Collider2D))]
    public class Gate : MonoBehaviour
    {
        Collider2D Collider2D;

        [HideInInspector] public bool DisableTriggerStay = false;

        public delegate void GateDelegate(Transform Object);
        public GateDelegate OnObjectInGate;
        public Collider2D Collider
        {
            get {
                return Collider2D;
            }
        }

        void Awake()
        {
            Collider2D = GetComponent<Collider2D>();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag != TagConstant.PLAYER)
                return;

            DisableTriggerStay = false;
        }

        void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag != TagConstant.PLAYER || DisableTriggerStay)
                return;

            Vector2 otherMin = other.bounds.min;
            Vector2 otherMax = other.bounds.max;
            Vector2 min = Collider2D.bounds.min;
            Vector2 max = Collider2D.bounds.max;

            if (null != OnObjectInGate
                    && otherMin.x >= min.x && otherMin.x <= max.x
                    && otherMax.x >= min.x && otherMax.x <= max.x
                    && otherMin.y >= min.y && otherMin.y <= max.y
                    && otherMax.y >= min.y && otherMax.y <= max.y)
                OnObjectInGate(other.transform);
        }
    }
}
