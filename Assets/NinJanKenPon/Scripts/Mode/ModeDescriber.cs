﻿using UnityEngine;
using NinJanKenPon.Enum;

namespace NinJanKenPon.ModeSystem
{
    [CreateAssetMenu(fileName = "NewMode", menuName = "NinJanKenPon/Mode")]
    public class ModeDescriber : ScriptableObject
    {
        public EnumMode Type;
        public Color Color;
        public Sprite Sign;
        public EnumMode[] Beat;
        public EnumMode[] BeatenBy;
    }
}
