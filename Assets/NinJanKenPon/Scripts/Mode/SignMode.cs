﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NinJanKenPon.ModeSystem
{
    public class SignMode : MonoBehaviour
    {
        const float UNLOAD_DEFAULT_TIME = 1f; // Equal to the speed parameter of the UnloadAnimation...

        [Header("General")]
        [SerializeField] float Speed = 0f;
        [SerializeField] Transform OrnmentList = null;
        [SerializeField] SpriteRenderer SignSprite = null;

        [Header("Ornment")]
        [SerializeField] int NumberOfOrnment = 8;
        [SerializeField] float OrnmentRadiusPosition = 1f;
        [SerializeField] GameObject OrnmentPrefab = null;

        [HideInInspector] public Transform Target = null;
        
        public delegate void SignModeDelegate();
        public SignModeDelegate OnUnload;

        float LoadTime = 0f;
        float CurrentLoadTime = 0f;
        bool IsUnloading = false;
        Animator Animator;

        float SignDeltaTime {
            get { return OrnmentList.childCount == 0 ? 0 : LoadTime / OrnmentList.childCount; }
        }
        int AngleDelta {
            get { return NumberOfOrnment == 0 ? 0 : 360 / NumberOfOrnment; }
        }

        void Awake()
        {
            Animator = GetComponent<Animator>();
            SpawnAllOrnment();
        }

        void Update()
        {
            Follow();
        }

        void FixedUpdate()
        {
            LoadOrnment();
        }
        void OnDrawGizmos()
        {
            if (null == Target)
                return;

            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(Target.position, 0.2f);
        }

        void Follow()
        {
            if (null == Target)
                return;

            Vector3 diff = transform.position - Target.position;
            transform.position -= diff * Time.deltaTime * Speed;
        }

        void SpawnAllOrnment()
        {
            float angle = 0;
            for (int i = 0; i < NumberOfOrnment; i++)
            {
                Quaternion q = Quaternion.AngleAxis(-1 * angle, Vector3.forward);
                Vector3 ornmentPosition = transform.position + q * Vector3.up * OrnmentRadiusPosition;

                Instantiate(OrnmentPrefab, ornmentPosition, Quaternion.identity, OrnmentList);

                angle += AngleDelta;
            }
        }

        void LoadOrnment()
        {
            if (CurrentLoadTime > LoadTime)
                return;

            int currentTime = (int)(CurrentLoadTime * 100);
            int deltaTime = (int)(SignDeltaTime * 100);
            int modulo = deltaTime == 0 ? 0 : currentTime % deltaTime;
            bool falsePositiveCase = modulo == 1 && currentTime == 1;

            if (!falsePositiveCase && (modulo == 0 || modulo == 1))
                ActiveNextOrnment();

            CurrentLoadTime += Time.fixedDeltaTime;
        }

        void ActiveNextOrnment()
        {
            foreach (Transform Ornment in OrnmentList)
                if (Ornment.gameObject.activeSelf)
                    continue;
                else {
                    Ornment.gameObject.SetActive(true);
                    break;
                }
        }

        public void Unload(float time)
        {
            if (IsUnloading)
                return;

            if (Debug.isDebugBuild)
                Debug.Log("[SignMode] Unload...");

            if (time == 0)
            {
                OnModeUnloadAnimationEnd();
                return;
            }

            IsUnloading = true;
            Animator.SetFloat("Time", UNLOAD_DEFAULT_TIME / time);
            Animator.SetTrigger("Unload");
        }

        void OnModeUnloadAnimationEnd() // Animation event
        {
            if (Debug.isDebugBuild)
                Debug.Log("[SignMode] Unload complete!");

            IsUnloading = false;

            if (null == OnUnload)
                return;

            OnUnload();
        }

        public void ResetToPosition()
        {
            transform.position = Target.position;
        }

        public void LoadSign(float time)
        {
            LoadTime = time;
            CurrentLoadTime = 0f;

            foreach (Transform Ornment in OrnmentList)
                Ornment.gameObject.SetActive(false);
        }

        public void UpdateSign(ModeDescriber mode)
        {
            SignSprite.sprite = mode.Sign;
            SignSprite.color = mode.Color;

            foreach (Transform Ornment in OrnmentList)
            {
                SpriteRenderer ornmentSpriteRenderer = Ornment.GetComponent<SpriteRenderer>();
                ornmentSpriteRenderer.color = mode.Color;
            }
        }
    }
}
